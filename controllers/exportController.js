const express = require('express')
const router = express.Router()
const isomatQueries = require('../queries/isomatQueries')
const fsManager = require('../managers/fsManager')

router.get('/', isomatQueries.exportIsomatJSON, fsManager.exportToZip, function (req, res) {

	const exportZipFile = './public/export.zip'

	res.setHeader('Content-disposition', 'attachment; filename=' + 'export')
  	res.setHeader('Content-type', 'application/zip')
	res.download(exportZipFile)
});
	
module.exports = router;
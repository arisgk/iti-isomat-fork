var express = require('express')
var router = express.Router()
var categoryQueries = require('../queries/categoryQueries')

router.get('/', (req, res, next) => {

	const searchText = req.param('searchText')

	if (searchText) {
    next('route')
  } else {
    next()
  }
}, categoryQueries.getCategories, (req, res) =>{

	var jsonResponse = {
		status: 'success',
		data: {
			serverTimestamp : Date.now(),
			categories: res.locals.categories,
			page: res.locals.page,
			pageCount: res.locals.pageCount,
			itemCount: res.locals.itemCount
		}
	}

  res.json(jsonResponse)
})

router.get('/', categoryQueries.searchCategoryByTitle, (req, res) => {

	var jsonResponse = {
		status: 'Searched category by title successfully.',
		data: {
			categories: res.locals.categories,
	}}
  res.json(jsonResponse);
});

router.post('/', categoryQueries.createNewCategory, (req, res) => {

	var jsonResponse = {'status': 'Created new category successfully.'}
	res.json(jsonResponse)
})

router.put('/:id', categoryQueries.updateCategory, (req, res) => {

	var jsonResponse = {'status': 'Updated category successfully' }
	res.json(jsonResponse)
})

module.exports = router

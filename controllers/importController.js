var express = require('express');
var router = express.Router();
var isomatQueries = require('../queries/isomatQueries');

router.get('/', isomatQueries.clearDatabaseEntries, isomatQueries.parseIsomatJSON, function (req, res) {

	var jsonResponse = {
		"status": "success",
		"data": {
			"message": 'Succesfully imported data from Isomat json.'
		}
	};
	res.json(jsonResponse);
});
	
module.exports = router;
function setup(app) {

	/*
		Triggers the custom 404 (not found) error handler in case no matching route was found.
		WARNING: This must be the last route declared, otherwise it will override routes declared after it.
		https://blog.safaribooksonline.com/2014/03/12/error-handling-express-js-applications/
	*/
	app.all('*', function(req, res, next) {
		var err = new Error();
		err.status = 404;
		err.message = 'The resource was not found.';
		return next(err);

	});

	// 401 (Not authorized) Error Handler
	app.use(function(err, req, res, next) {

		console.log('Error in request: ' + req.originalUrl + ', os: ' + req.headers['x-ostype'] + ', os version: ' + req.headers['x-osversion'] + ', app version: ' + req.headers['x-appversion'] + ', web id: ' + req.headers['x-webid']);

		console.log('Entered 401 error handler with error status : '+err.status);

		if(err.status !== 401) {
			console.log('Error status not equal to 401,moving on to next error handler');
			return next(err);
		}

		console.log('Continues at 401 error handler with status : '+err.status);
		console.log('-----------------');

		var jsonResponse = {
			"status": "fail",
			"data"  : {
				"message": "Not authorized"
			}
		};

		return res.status(err.status).send(jsonResponse);

	});

	// 404 (Not found) Error Handler
	app.use(function(err, req, res, next) {

		console.log('Entered 404 error handler');

		if(err.status !== 404) {
			console.log('Error status not equal to 404,moving on to next error handler');
			return next(err);
		}

		console.log("Continued to 404 handler");
		console.log('-----------------');

		var jsonResponse = {
			"status": "fail",
			"data"  : {
				"message": "The resource was not found"
			}
		};


		return res.status(err.status).send(jsonResponse);

	});

	// Generic 4xx Error Handler
	app.use(function(err, req, res, next) {

		var pattern = /^[4][0-9][0-9]$/;

		console.log("Entered 4xx generic error handler with status : " + err.status);

		if (pattern.test(err.status) === false) {
			return next(err);
		}

		console.log("Continued to 4xx handler");
		console.log('-----------------');

		if (!err.message) {
			err.message = "Client Error";
		}

		var jsonResponse = {
			"status": "fail",
			"data"  : {
				"message": err.message,
				"stack": err.stack
			}
		};

		return res.status(err.status).send(jsonResponse);
	});

	// 504 (Gateway Timeout) Error Handler
	app.use(function(err, req, res, next) {

		console.log("Entered 504 handler with error status : "+err.status);

		if(err.status !== 504) {
			console.log('Error status not equal to 504,moving on to next error handler');
			return next(err);
		}

		if (!err.message) {
			err.message = "Gateway Timeout";
		}
		console.log("Continued to 504 handler");
		console.log('-----------------');

		var jsonResponse = {
			"status": "error",
			"message": err.message,
			"data": {
				"code": 504,
				"stack": err.stack
			}
		};

		return res.status(err.status).send(jsonResponse);

	});

	// Generic 5xx Error Handler
	app.use(function(err, req, res, next) {
		console.log(err);
		console.log("Entered generic 5xx handler");

		// if ((err.status != /^[5][0-9][0-9]$/) || (err.status === 500)) {
		// 	console.log('mpike');
		// 	return next(err);
		// }

		console.log("Continued to 5xx handler");
		console.log('-----------------');

		if (!err.status) {
			err.status = 500;
		}

		if (!err.message) {
			err.message = "Internal Server Error";
		}

		var jsonResponse = {
			"status": "error",
			"message": err.message,
			"data": {
				"code": err.status
			}
		};

		return res.status(err.status).send(jsonResponse);
	});
}

module.exports.registerHandlers = setup;

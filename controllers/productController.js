var express = require('express')
var router = express.Router()
var productQueries = require('../queries/productQueries')

router.get('/', (req, res, next) => {

	const searchText = req.param('searchText')

	if (searchText) {
    next('route')
  } else {
    next()
  }
}, productQueries.getProducts, (req, res) => {

	var jsonResponse = {
		status: 'success',
		data: {
			serverTimestamp : Date.now(),
			products: res.locals.products,
			page: res.locals.page,
			pageCount: res.locals.pageCount,
			itemCount: res.locals.itemCount
		}
	}

  res.json(jsonResponse)
})

router.get('/', productQueries.searchProductByTitle, (req, res) => {

	var jsonResponse = {
		status: 'Searched product by title successfully.',
		data: {
			products: res.locals.products,
		}
	}
  res.json(jsonResponse);
});

router.post('/', productQueries.createNewProduct, (req, res) => {

	var jsonResponse = {'status': 'Created new product successfully.'}
	res.json(jsonResponse)
})

router.put('/:id', productQueries.updateProduct, (req, res) => {

	var jsonResponse = {'status': 'Updated product successfully' }
	res.json(jsonResponse)
})

router.delete('/:id', productQueries.deleteProduct, (req, res) => {

	var jsonResponse = {'status': 'Deleted product successfully' }
	res.json(jsonResponse)
})

module.exports = router

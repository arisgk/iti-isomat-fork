var express = require('express')
var router = express.Router()
var solutionQueries = require('../queries/solutionQueries')
var fsManager = require('../managers/fsManager')

router.get('/', solutionQueries.getSolutions, (req, res) => {

	var jsonResponse = {
		"status": "success",
		"data": {
			"solutions": res.locals.solutions,
		}
	}

	res.json(jsonResponse)
})

router.get('/:id', solutionQueries.getSolution, (req, res) => {

	var jsonResponse = {
		"status": "success",
		"data": {
			"solution": res.locals.solution,
		}
	}

	res.json(jsonResponse)
})

router.post('/', solutionQueries.createNewSolution, (req, res) => {

	var jsonResponse = {'status': 'Created new solution successfully.'}
	res.json(jsonResponse)
})

router.put('/:id', solutionQueries.updateSolution, (req, res) => {
    var jsonResponse = {'status': 'Updated category successfully.'}
	res.json(jsonResponse)
})

router.post('/:id/images', fsManager.uploadImage(), solutionQueries.addImage, (req, res) => {
    var jsonResponse = {
        status: 'Added image successfully.',
        data: {
            solution: res.locals.solution
        }
    }
	res.json(jsonResponse)
})

router.delete('/:id/images', solutionQueries.deleteImage, fsManager.deleteImage, (req, res) => {
    var jsonResponse = {
        status: 'Deleted image successfully.',
        data: {
            solution: res.locals.solution
        }
    }
	res.json(jsonResponse)
})

module.exports = router

var path = require('path');
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    target: 'web',

    entry: [
        './public/index.js'
    ],

    output: {
        path: path.join(process.cwd(), '/public'),
        pathInfo: true,
        publicPath: 'http://localhost:3000/public/',
        filename: 'index.bundle.js'
    },

    resolve: {
        root: path.join(__dirname, ''),
        modulesDirectories: [
            'node_modules',
        ],
        extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx']
    },

    plugins: [
        new CopyWebpackPlugin([{
            from: 'node_modules/bootstrap/dist/css/*',
            to: 'stylesheets/bootstrap/css',
            flatten: true
        }]),
        new webpack.ContextReplacementPlugin(
            /uploads$/, /^$/
        )
    ],

    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
        }, {
            test: /\.scss$/,
            loaders: ["style", "css", "sass"]
        }, {
            test: /\.(png|jpg|jpeg|gif|woff|woff2|eot|ttf|svg)$/,
            loader: 'url-loader',
        }],

        // Disable handling of requires with a single expression
        exprContextRegExp: /$^/,
        exprContextCritical: false,
    }
};

var mongoose = require('mongoose');
var mongoosePaginate = require('../custom_modules/mongoosePaginateCustom');

var categorySchema = mongoose.Schema({
	id: Number,
	parentid: Number,
	color: String,
	bgcolor: String,
	language: String,
	title: String,
	children: [Number],
	products: String,
	ordering: String
});

categorySchema.plugin(mongoosePaginate);
categorySchema.index( { title: "text" } )

module.exports = mongoose.model('Category', categorySchema, 'Categories');

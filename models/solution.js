var mongoose = require('mongoose');
var mongoosePaginate = require('../custom_modules/mongoosePaginateCustom');

var solutionSchema = mongoose.Schema({
	id: Number,
	title: String,
	description: String,
	images: [String],
    video: String,
});

solutionSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Solution', solutionSchema, 'Solutions');

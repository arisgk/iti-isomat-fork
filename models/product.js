var mongoose = require('mongoose');
var mongoosePaginate = require('../custom_modules/mongoosePaginateCustom');

var productSchema = mongoose.Schema({
	id: Number,
	catid: [Number],
	pdf: String,
	pdfLocal: String,
	image: String,
	imageLocal: String,
	video: String,
	videoLocal: String,
	title: String,
	description: String,
	properties: String,
	shade : {
    link: String,
    text: String
  },
  consumption: String,
	package: String,
	language: String,
	deleted: Boolean
});

productSchema.plugin(mongoosePaginate);
productSchema.index( { title: "text" } )

module.exports = mongoose.model('Product', productSchema, 'Products');

# README #

ITI - Isomat Web Server
=======================

Building & Running the Server
-----------------------------
### Prerequisites
[Node.js](https://nodejs.org/)  
[Node Foreman](https://github.com/strongloop/node-foreman) to read your environment and start Node server  
[Webpack](https://webpack.github.io/) to bundle your front-end modules

### Environment
The following environment variables are required: __DATABASE_URI__, __ISOMAT_URL__ and __PORT__. Node Foreman registers these variables with `nf start` if a *.env* file with them exists in the project root.

### Start the server
Open your terminal and navigate to the project root folder. Run the following commands:
```
npm install
webpack
nf start
```

Package Manager
---------------
[npm](https://www.npmjs.com/) is used as a package manager for both the back-end and the front-end. All project dependencies exist in the *package.json* file on the project root folder.

Backend
-------
Built using [Node.js](https://nodejs.org/) and [Express](http://expressjs.com/) framework.

Node Foreman reads the process file (*Procfile*) from the project root folder. The Procfile defines a __web__ process that starts the server when you run `nf start`. In our case, the web process is the one created when Node executes the *config/app.js* file, as described in the Procfile.

In *config/app.js* we first ensure that the imported files directories are properly created and upon successful creation we register the RESTful API controllers (route handler modules).

The root controller (*controllers/rootController.js*) serves the index.html page using the [ejs](http://www.embeddedjs.com/) template language. Once served, ReactJS takes over.

Database
--------
The database used is [MongoDB](https://www.mongodb.com/) and is exposed to the server via the __DATABASE_URI__ environment variable.

[mongoose](http://mongoosejs.com/) is used to model our application data and interact with the database.

User Interface
--------------
Designed according to [Google Material Design](https://www.google.com/design/spec/material-design/introduction.html) guidelines.

Frontend
--------
Built using [React](https://facebook.github.io/react/) and [Redux](http://redux.js.org/).

Webpack bundles all our front-end JavaScript files in the *public/index.bundle.js* file when `webpack` command is run. This bundle is then loaded in our root page (*index.html*) by adding it as a script element in the *index.ejs* file.

In order to use the new [ES6](http://babeljs.io/docs/plugins/preset-es2015/) (ES2015) and ES7 JavaScript syntax, we use [Babel](https://babeljs.io/). Webpack then uses [babel-loader](https://github.com/babel/babel-loader) to transform the new syntax into the old one that all browsers already understand when bundling our JavaScript files.

React root component is instantiated in *public/index.js* and is connected to the Redux store using react-redux's Provider component.

The folder structure on the front-end follows the Redux examples structure, thus a good understanding of Redux is needed. The [Redux documentation](http://redux.js.org/) and the [Getting Started with Redux](https://egghead.io/series/getting-started-with-redux) course (made by Redux's creator, Dan Abramov) are some great resources to start.

[Material-UI](http://www.material-ui.com/), a set of React components that implement Material design, is used throughout the app.

Routing
-------
The web app is essentially a Single Page Application (SPA) and all routing is handled on the front-end. The web server always serves `/` and [react-router](https://github.com/reactjs/react-router) handles reading the URL and rendering the correct React components for each section.

### Add New Route
New routes can be added from the root React presentational component (*public/components/Root/index.js*). Add every new route and the React component that will handle it there. To add the new route to the navigation drawer, add a respective `MenuItem` in *public/components/Layout/index.js* and handle its click event.

RESTFul API
-----------
The web server RESTFul API routes are prefixed with `/api`. When a React component needs some data from the web server it triggers Redux actions, which in turn make asynchronous API calls and retrieve the data as an HTTP response in JSON format.

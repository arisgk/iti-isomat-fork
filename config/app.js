const express = require('express')
const db = require('./db')()
const app = express()
const server = app.listen(process.env.PORT)
server.timeout = 3600000
const fsManager = require('../managers/fsManager')
const isomatQueries = require('../queries/isomatQueries');
const path = require('path')
const bodyParser = require('body-parser');

fsManager.ensureDirectories( (err) => {

    if (err) {

        console.log(err)
        return err
    }

    console.log('created directories successfully')

    // Set static assets directory (public)
    app.use(express.static(path.join(__dirname, '../public')));

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    // Set views directory
    app.set('views', path.join(__dirname, '../views'));

    // Set view engine
    app.set('view engine', 'ejs');

    var importController = require('../controllers/importController');
    app.use('/api/import', importController);

    var exportController = require('../controllers/exportController');
    app.use('/api/export', exportController);

    var categoryController = require("../controllers/categoryController");
    app.use("/api/categories", categoryController);

    var productController = require("../controllers/productController");
    app.use("/api/products", productController);

    var solutionController = require("../controllers/solutionController");
    app.use("/api/solutions", solutionController);

    var rootController = require('../controllers/rootController');
    app.use('/', rootController);
})

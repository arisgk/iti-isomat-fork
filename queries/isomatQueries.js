const async = require('async')
const request = require('request')
const Category = require('../models/category')
const categoryQueries = require('./categoryQueries')
const productQueries = require('./productQueries')
const solutionQueries = require('./solutionQueries')
const fsManager = require('../managers/fsManager')

var queries = {};

queries.clearDatabaseEntries = function (req, res, next){

    async.parallel([

        (callback) => {

            categoryQueries.removeAllCategories(callback);
        },
        (callback) => {

            productQueries.removeAllProducts(callback);
        }
    ],
    (err, results) => {

        if (err){

            err.message = 'Error in clearing database entries.';
            err.status = 500;
            return next(err);
        } else {

            console.log('Database entries cleared successfully.');
            next();
        }
    });
}

queries.parseIsomatJSON = function (req, res, next) {

    var url = process.env.ISOMAT_URL;

    request({
        url: url,
        json: true
    }, function (error, response, body) {

        if (!error && response.statusCode === 200) {

            async.parallel([
                function(callback){

                    categoryQueries.saveCategories(body.data.categories, callback);
                },
                function(callback){

                    productQueries.saveProducts(body.data.products, callback);
                }
            ],
            function(err, results){

                if (err){

                    err.message = 'Error in saving isomat JSON.';
                    err.status = 500;
                    return next(err);
                } else {

                    console.log('Isomat JSON save successful.');
                    next();

                }
            });
        } else {

            error.message = 'Error in trying to fetch ISOMAT JSON url.';
            error.status = 500;
            return next(error);
        }
    });
};

queries.exportIsomatJSON = function (req, res, next){

    const exportJson = {};

    exportJson.created_ts = Date.now();

    exportJson.data = {};
    exportJson.data.categories = [];
    exportJson.data.products = [];
    exportJson.data.solutions = [];

    async.parallel([
        callback => {
            categoryQueries.getAllCategories(exportJson, callback);
        },
        callback =>{
            productQueries.getAllProducts(exportJson, callback);
        },
        callback =>{
            solutionQueries.getAllSolutions(exportJson, callback);
        }
    ],
    (err, results) => {
        if (err){

            err.message = 'Error in saving isomat export JSON.';
            err.status = 500;
            return next(err);
        } else {

            console.log('Isomat export JSON created successfully.');
            res.locals.exportJson = exportJson;
            fsManager.writeExportToJsonFile(exportJson, next);
        }
    });
}

module.exports = queries;

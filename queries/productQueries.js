"use strict"

const async = require('async');
const Product = require('../models/product');
const fsManager = require('../managers/fsManager');

let queries = {};

queries.searchProductByTitle = (req, res, next) => {

    const searchText = req.param('searchText')
    const queryParams = { title: { $regex: searchText, $options: 'i' } }

    Product.find(queryParams, (error, products) => {

      if (error) {
        error.status = 500
        error.message = 'Error fetching products by text search.'
        return next(error)
      }

      res.locals.products = products
      return next()
    })
}

queries.removeAllProducts = function (asyncCallback){

    Product.remove({}, function (err) {

        if (err) {

            err.message = 'Error in removing products.';
            err.status = 500;
            return asyncCallback(err);
        } else {

            console.log('Products removed successfully.');
            asyncCallback();
        }
    });
}

queries.saveProducts = function (products, asyncCallback){

    async.eachSeries(products, function(product, callback) {

        saveIndividualProduct(product, callback);
    }, function(err){

        if (err) {

            err.message = 'Error in saving products.';
            err.status = 500;
            return asyncCallback(err);
        } else {

            console.log('Products saved successfully.');
            asyncCallback();
        }
    });
}

var saveIndividualProduct = function (product, asyncCallback){

    var newProduct = new Product();

    if (product.id){
        newProduct.id = product.id;
    }
    if (product.catid){
        newProduct.catid = product.catid;
    }
    if (product.pdf){
        newProduct.pdf = product.pdf;
    }
    if (product.image){
        newProduct.image = product.image;
    }
    if (product.title){
        newProduct.title = product.title;
    }
    if (product.description){
        newProduct.description = product.description;
    }
    if (product.properties){
        newProduct.properties = product.properties;
    }
    if (product.shade && product.shade.link){
        newProduct.shade.link = product.shade.link;
    }
    if (product.text && product.shade.text){
        newProduct.shade.text = product.shade.text;
    }
    if (product.consumption){
        newProduct.consumption = product.consumption;
    }
    if (product.package){
        newProduct.package = product.package;
    }
    if (product.language){
        newProduct.language = product.language;
    }
    if (product.deleted){
        newProduct.deleted = product.deleted;
    }

    newProduct.save(function (err) {

        if (err) {

            err.message = 'Error in saving product.';
            err.status = 504;
            return asyncCallback(err);
        } else {

            fsManager.saveFilesLocally(newProduct, asyncCallback);
        }
    });
}

queries.getProducts = function (req, res, next) {

    // Get page and items from the request params.
    var page = req.param('page');
    var items = req.param('items');

    // Get offset from request params (the value that will be skipped in our query) and check its validity too (default it to 0).
    var offsetString = req.param('offset');
    var offset = parseInt(offsetString);
    if (isNaN(offset)){
        offset = 0;
    }

    if (page && items) {

        Product.paginate( {}, {}, page, items, offset, function(err, pageCount, paginatedResults, itemCount) {

            if (err) {
                console.log(err)
                err.status = 500
                err.message = 'Failed to get products from database'
                return next(err)
            }

            res.locals.products = paginatedResults
            res.locals.page = parseInt(page)
            res.locals.pageCount = pageCount
            res.locals.itemCount = itemCount

            next()
        })
    } else {
        // Throw 400 (Bad request) error, since page and/or items do not exist in request params.
        var err = new Error();
        err.status  = 400;
        err.message = 'Page number and/or items number not specified in request.';
        next(err);
    }
}

queries.createNewProduct = function (req, res, next){

    var product = new Product()

    var id = req.body.id;
    if (id) {
        product.id = id;
    }

    var title = req.body.title;
    if (title) {
        product.title = title;
    }

    var catid = req.body.catid;
    if (catid) {
        product.catid = catid;
    }

    var pdf = req.body.pdf;
    if (pdf) {
        product.pdf = pdf;
    }

    var image = req.body.image;
    if (image) {
        product.image = image;
    }

    var description = req.body.description;
    if (description) {
        product.description = description;
    }

    var properties = req.body.properties;
    if (properties) {
        product.properties = properties;
    }

    var shade = req.body.shade;
    if (shade) {

        product.shade = {}

        if (shade.link) {
            product.shade.link = shade.link
        }

        if (shade.text) {
            product.shade.text = shade.text
        }
    }

    var pdf = req.body.pdf;
    if (pdf) {
        product.pdf = pdf;
    }

    var consumption = req.body.consumption;
    if (consumption) {
        product.consumption = consumption;
    }

    var productPackage = req.body.package;
    if (productPackage) {
        product.package = productPackage;
    }

    var language = req.body.language;
    if (language) {
        product.language = language;
    }

    product.deleted = false;

    product.save(function (err) {

        if (err) {

            err.message = 'Error in saving product.'
            err.status = 504
            return next(err)
        } else {
            console.log('Saved new product successfully')
            next()
        }
    });
}

queries.updateProduct = function (req, res, next){

    const productID = req.params.id
    const updatedProduct = req.body

    if (!productID.toString().match(/^[0-9a-fA-F]{24}$/)) {

        err = new Error();
        err.message = 'Invalid product ID.';
        err.status = 400;
        return next(err);
    }

    if (updatedProduct){

        Product.findById(productID, function (err, product){

            if (err) {
                err.status = 500;
                err.message = 'Failed to get product from database';
                return next(err);
            } else {

                updateProduct(product, updatedProduct, next);
            }
        })
    } else {

        let err = new Error();
        err.message = 'Invalid product';
        err.status = 400;
        return next(err);
    }
}

const updateProduct = function (product, updatedProduct, next){

    if (updatedProduct.id){
        product.id = updatedProduct.id
    }
    if (updatedProduct.catid){
        product.catid = updatedProduct.catid
    }
    if (updatedProduct.pdf){
        product.pdf = updatedProduct.pdf
    }
    if (updatedProduct.image){
        product.image = updatedProduct.image
    }
    if (updatedProduct.title){
        product.title = updatedProduct.title
    }
    if (updatedProduct.description){
        product.description = updatedProduct.description
    }
    if (updatedProduct.properties){
        product.properties = updatedProduct.properties
    }
    if (updatedProduct.shade && updatedProduct.shade.link){
        product.shade.link = updatedProduct.shade.link
    }
    if (updatedProduct.text && updatedProduct.shade.text){
        product.shade.text = updatedProduct.shade.text
    }
    if (updatedProduct.consumption){
        product.consumption = updatedProduct.consumption
    }
    if (updatedProduct.package){
        product.package = updatedProduct.package
    }
    if (updatedProduct.language){
        product.language = updatedProduct.language
    }
    if (updatedProduct.deleted){
        product.deleted = updatedProduct.deleted
    }

    product.save(function (err) {

        if (err) {
            err.status  = 500
            err.message = 'Error in updating product.'
            return next(err)
        } else {

            console.log('Updated product successfully.')
            next()
        }
    });
}

queries.deleteProduct = function (req, res, next){

    const productID = req.params.id

    if (!productID.toString().match(/^[0-9a-fA-F]{24}$/)) {

        err = new Error()
        err.message = 'Invalid product ID.'
        err.status = 400
        return next(err)
    }

    Product.findOneAndUpdate({'_id': productID}, { 'deleted': true }, function (err, product) {

        if (err) {

            err.status = 500
            err.message = 'Failed to delete product from database'
            return next(err)
        } else {

            console.log('Deleted product successfully.')
            next()
        }
    });
}

queries.getAllProducts = function (exportJson, asyncCallback){

    Product.find({}, (err, products) =>{

        if (err) {

            err.message = 'Error in fetching total products.';
            err.status = 500;
            return asyncCallback(err);
        } else {

            console.log('Total products fetched successfully.');
            exportJson.data.products = products;
            asyncCallback();
        }
    })
}

module.exports = queries;

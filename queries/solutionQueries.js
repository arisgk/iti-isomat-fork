"use strict"
var async = require('async');
var Solution = require('../models/solution');

var queries = {};

queries.getSolutions = function (req, res, next) {

    Solution.find({}, (err, solutions) => {

        if (err) {

            err.message = 'Error in fetching solutions.'
            err.status = 500
            return next(err)
        } else {

            res.locals.solutions = solutions
            next()
        }
    })
}

queries.getSolution = function (req, res, next) {

    const solutionID = req.params.id

    if (!solutionID.toString().match(/^[0-9a-fA-F]{24}$/)) {

        var err = new Error()
        err.message = 'Invalid solution ID.'
        err.status = 400
        return next(err)
    }

    Solution.findOne({'_id': solutionID}, function (err, solution) {

        if (err) {

            err.status = 500
            err.message = 'Failed to get solution from database'
            return next(err)
        } else {
            res.locals.solution = solution
            next()
        }
    });
}

queries.updateSolution = function (req, res, next) {

    var _id = req.params.id

    if (!_id.toString().match(/^[0-9a-fA-F]{24}$/)) {

        err = new Error()
        err.message = 'Invalid solution ID.'
        err.status = 400
        return next(err)
    }

    var id = req.body.id || ''
    var title = req.body.title || ''
    var description = req.body.description || ''
    var images = req.body.images || []
    var video = req.body.video || ''

    Solution.findOneAndUpdate(
        { '_id': _id },
        { $set: {
            'id': id,
            'title': title,
            'description': description,
            'images': images,
            'video': video
        } },
        { new: true },
        function(err, solution) {

            if (err) {
                err.status = 500
                err.message = 'Failed to update solution'
                return next(err)
            }

            next()
        }
    )
}

queries.createNewSolution = function (req, res, next){

    var solution = new Solution()

    var id = req.body.id
    if (id) {
        solution.id = id
    }

    var title = req.body.title
    if (title) {
        solution.title = title
    }

    var description = req.body.description
    if (description) {
        solution.description = description
    }

    var images = req.body.images
    if (images && images.length > 0) {
        solution.images = images
    } else {
        solution.images = []
    }

    var video = req.body.video
    if (video) {
        solution.video = video
    }

    solution.save(function (err) {

        if (err) {

            err.message = 'Error in saving solution.'
            err.status = 504
            return next(err)
        } else {
            next()
        }
    });
}

queries.getAllSolutions = function (exportJson, asyncCallback){

    Solution.find({}, (err, solutions) =>{

        if (err) {

            err.message = 'Error in fetching total solutions.'
            err.status = 500
            return asyncCallback(err)
        } else {

            console.log('Total solutions fetched successfully.')
            exportJson.data.solutions = solutions
            asyncCallback()
        }
    })
}

queries.addImage = function (req, res, next) {

    var _id = req.params.id

    if (!_id.toString().match(/^[0-9a-fA-F]{24}$/)) {

        err = new Error()
        err.message = 'Invalid solution ID.'
        err.status = 400
        return next(err)
    }

    Solution.findOneAndUpdate(
        { '_id': _id },
        { $push: {
            'images': req.file.path,
        } },
        { new: true },
        function(err, solution) {

            if (err) {
                err.status = 500
                err.message = 'Failed to add image to solution in db'
                return next(err)
            }

            res.locals.solution = solution;

            next()
        }
    )
}

queries.deleteImage = function (req, res, next) {

    var _id = req.params.id

    if (!_id.toString().match(/^[0-9a-fA-F]{24}$/)) {

        err = new Error()
        err.message = 'Invalid solution ID.'
        err.status = 400
        return next(err)
    }

    Solution.findOneAndUpdate(
        { '_id': _id },
        { $pull: {
            'images': req.query.image,
        } },
        { new: true },
        function(err, solution) {

            if (err) {
                err.status = 500
                err.message = 'Failed to delete image from solution in db'
                return next(err)
            }

            res.locals.solution = solution;

            next()
        }
    )
}

module.exports = queries;

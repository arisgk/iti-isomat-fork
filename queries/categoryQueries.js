"use strict"
var async = require('async');
var Category = require('../models/category');

var queries = {};

queries.searchCategoryByTitle = (req, res, next) => {

    const searchText = req.param('searchText')
    const queryParams = { title: { $regex: searchText, $options: 'i' } }

    Category.find(queryParams, (error, categories) => {

      if (error) {
        error.status = 500
        error.message = 'Error fetching categories by text search.'
        return next(error)
      }

      res.locals.categories = categories
      return next()
    })
}

queries.removeAllCategories = function (asyncCallback){

    Category.remove({}, function (err){

        if (err) {

            err.message = 'Error in removing categories.';
            err.status = 500;
            return asyncCallback(err);
        } else {

            console.log('Categories removed successfully.');
            asyncCallback();
        }
    });
}

queries.saveCategories = function (categories, asyncCallback){

    async.each(categories, function(category, callback) {

        saveIndividualCategory(category, callback);
    }, function(err){

        if (err) {

            err.message = 'Error in saving categories.';
            err.status = 500;
            console.log('Error ' + err.message);
            return asyncCallback(err);
        } else {

            console.log('Categories saved successfully.');
            asyncCallback();
        }
    });
}

var saveIndividualCategory = function (category, asyncCallback){

    var newCategory = new Category();

    if (category.id){
        newCategory.id = category.id;
    }
    if (category.parentid){
       newCategory.parentid = category.parentid;
    }
    if (category.color){
        newCategory.color = category.color;
    }
    if (category.color){
        newCategory.bgcolor = category.bgcolor;
    }
    if (category.language){
        newCategory.language = category.language;
    }
    if (category.title){
        newCategory.title = category.title;
    }
    if (category.children){
        newCategory.children = category.children;
    }
    if (category.products){
        newCategory.products = category.products;
    }
    if (category.ordering){
         newCategory.ordering = category.ordering;
    }

    newCategory.save(function (err) {

        if (err) {

            err.message = 'Error in saving category.';
            err.status = 504;
            return asyncCallback(err);
        } else {

            asyncCallback();
        }
    });
}

queries.getCategories = function (req, res, next) {

    // Get page and items from the request params.
    var page = req.param('page');
    var items = req.param('items');

    // Get offset from request params (the value that will be skipped in our query) and check its validity too (default it to 0).
    var offsetString = req.param('offset');
    var offset = parseInt(offsetString);
    if (isNaN(offset)){
        offset = 0;
    }

    if (page && items) {

        Category.paginate( {}, {}, page, items, offset, function(err, pageCount, paginatedResults, itemCount) {

            if (err) {
                console.log(err)
                err.status = 500
                err.message = 'Failed to get categories from database'
                return next(err)
            }

            res.locals.categories = paginatedResults
            res.locals.page = parseInt(page)
            res.locals.pageCount = pageCount
            res.locals.itemCount = itemCount

            next()
        })
    } else {
        // Throw 400 (Bad request) error, since page and/or items do not exist in request params.
        var err = new Error();
        err.status  = 400;
        err.message = 'Page number and/or items number not specified in request.';
        next(err);
    }
}

queries.getAllCategories = function (exportJson, asyncCallback){

    Category.find({}, (err, categories) =>{

        if (err) {

            err.message = 'Error in fetching total categories.'
            err.status = 500
            return asyncCallback(err)
        } else {

            console.log('Total categories fetched successfully.')
            exportJson.data.categories = categories
            asyncCallback()
        }
    })
}

queries.createNewCategory = function (req, res, next){

    var category = new Category()

    var id = req.body.id
    if (id) {
        category.id = id
    }

    var parentid = req.body.parentid
    if (parentid) {
        category.parentid = parentid
    }

    var color = req.body.color
    if (color) {
        category.color = color
    }

    var bgcolor = req.body.bgcolor
    if (bgcolor) {
        category.bgcolor = bgcolor
    }

    var language = req.body.language
    if (language) {
        category.language = language
    }

    var title = req.body.title
    if (title) {
        category.title = title
    }

    var children = req.body.children
    if (children) {
        category.children = children
    }

    var products = req.body.products
    if (products) {
        category.products = products
    }

    var ordering = req.body.ordering
    if (ordering) {
        category.ordering = ordering
    }

    category.save(function (err) {

        if (err) {

            err.message = 'Error in saving category.'
            err.status = 504
            return next(err)
        } else {
            console.log('Saved new category successfully')
            next()
        }
    });
}

queries.updateCategory = function (req, res, next){

    const categoryID = req.params.id
    const updatedCategory = req.body

    if (!categoryID.toString().match(/^[0-9a-fA-F]{24}$/)) {

        err = new Error()
        err.message = 'Invalid product ID.'
        err.status = 400
        return next(err)
    }

    if (updatedCategory){

        Category.findById(categoryID, function (err, category){

            if (err) {
                err.status = 500
                err.message = 'Failed to get category from database'
                return next(err)
            } else {

                updateCategory(category, updatedCategory, next)
            }
        })
    } else {

        let err = new Error()
        err.message = 'Invalid category'
        err.status = 400
        return next(err)
    }
}

const updateCategory = function (category, updatedCategory, next){

    if (updatedCategory.id){
        category.id = category.id
    }
    if (updatedCategory.parentid){
        category.parentid = updatedCategory.parentid
    }
    if (updatedCategory.color){
        category.color = updatedCategory.color
    }
    if (updatedCategory.bgcolor){
        category.bgcolor = updatedCategory.bgcolor
    }
    if (updatedCategory.language){
        category.language = updatedCategory.language
    }
    if (updatedCategory.title){
        category.title = updatedCategory.title
    }
    if (updatedCategory.children){
        category.children = updatedCategory.children
    }
    if (updatedCategory.products){
        category.products = updatedCategory.products
    }
    if (updatedCategory.ordering){
        category.ordering = updatedCategory.ordering
    }

    category.save(function (err) {

        if (err) {
            err.status  = 500
            err.message = 'Error in updating category.'
            return next(err)
        } else {

            console.log('Updated category successfully.')
            next()
        }
    });
}

queries.deleteCategory = function (req, res, next){

    const categoryID = req.params.id

    if (!categoryID.toString().match(/^[0-9a-fA-F]{24}$/)) {

        err = new Error()
        err.message = 'Invalid category ID.'
        err.status = 400
        return next(err)
    }

    Category.remove({'_id': categoryID}, function (err, product) {

        if (err) {

            err.status = 500
            err.message = 'Failed to delete category from database'
            return next(err)
        } else {

            console.log('Deleted category successfully.')
            next()
        }
    });
}

module.exports = queries;

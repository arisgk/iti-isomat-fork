import { connect } from 'react-redux'
import SolutionDetails from '../../components/Solutions/SolutionDetails'
import {
    getSolutionFromAPI,
    updateSolution,
    uploadImage,
    deleteImage,
} from '../../actions/solutionActions.js'

const getSolution = (id, solutions) => {
    if (solutions && solutions.data) {
        const data = solutions.data
        for (let solution of data) {
            if (solution._id == id) return solution
        }
    }

    return null
}

const mapStateToProps = (state, ownProps) => {
    return {
        solution: getSolution(ownProps.params.id, state.solutions),
        isUpdatingSolution: state.solutions.isUpdatingSolution,
        params: ownProps.params
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSolution: (id) => {
            dispatch(getSolutionFromAPI(id))
        },
        onSave: (originalSolution, updatedSolution) => {
            dispatch(updateSolution(originalSolution, updatedSolution))
        },
        onImageDrop: (file, solution) => {
            dispatch(uploadImage(file, solution))
        },
        onImageDelete: (image, solution) => {
            dispatch(deleteImage(image, solution))
        },
    }
}

const EditableSolution = connect(
    mapStateToProps,
    mapDispatchToProps
)(SolutionDetails)

export default EditableSolution

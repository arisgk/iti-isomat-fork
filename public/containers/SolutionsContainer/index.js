import { connect } from 'react-redux'
import Solutions from '../../components/Solutions'
import {
    getSolutions,
    updateSolution
} from '../../actions/solutionActions.js'


const mapStateToProps = (state) => {
    return {
        solutions: state.solutions
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: () => {
            dispatch(getSolutions())
        },
    }
}

const SolutionsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Solutions)

export default SolutionsContainer

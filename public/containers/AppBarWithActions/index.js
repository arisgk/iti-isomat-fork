import { connect } from 'react-redux'
import LogoTitleAppBar from '../../components/LogoTitleAppBar'
import {
    importData,
    startExporting,
    completedExporting,
    search,
    resetSearch,
    getSearchSuggestions,
} from '../../actions/appBarActions.js'

const mapStateToProps = (state) => {
    return {
        importing: state.appBar.importing,
        exporting: state.appBar.exporting,
        searchSuggestions: state.appBar.searchSuggestions,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onImportClick: () => {
            dispatch(importData())
        },
        onExportClick: () => {
            dispatch(startExporting())
        },
        onDownloadComplete: () => {
            dispatch(completedExporting())
        },
        onSearch: (string) => {
            if (ownProps.currentRoute === '/categories' || ownProps.currentRoute === '/') {
                dispatch(search(string, 'categories'))
            } else {
                dispatch(search(string, 'products'))
            }
        },
        onSearchReset: () => {
            if (ownProps.currentRoute === '/categories' || ownProps.currentRoute === '/') {
                dispatch(resetSearch('categories'))
            } else {
                dispatch(resetSearch('products'))
            }
        },
        onSearchTyping: (string) => {
            if (ownProps.currentRoute === '/categories' || ownProps.currentRoute === '/') {
                dispatch(getSearchSuggestions(string, 'categories'))
            } else {
                dispatch(getSearchSuggestions(string, 'products'))
            }
        }
    }
}

const Products = connect(
    mapStateToProps,
    mapDispatchToProps
)(LogoTitleAppBar)

export default Products

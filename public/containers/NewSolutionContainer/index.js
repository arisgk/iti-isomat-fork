import { connect } from 'react-redux'
import NewSolution from '../../components/Solutions/NewSolution'
import { saveNewSolution } from '../../actions/solutionActions.js'

const mapStateToProps = (state) => {
    return {
        isAddingSolution: state.isAddingSolution,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSaveNew: (solution, router) => {
            dispatch(saveNewSolution(solution, router))
        },
    }
}

const NewSolutionContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(NewSolution)

export default NewSolutionContainer

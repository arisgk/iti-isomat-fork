import { connect } from 'react-redux'
import Categories from '../../components/Categories'
import {
    getFirstCategoriesPage,
    getPreviousCategoriesPage,
    getNextCategoriesPage,
    showAddCategoryDialog,
    selectCategory,
    clearCategorySelection,
    deleteCategory,
    startEditingSelectedCategory,
    updateCategory,
    cancelEditingCategory,
    saveNewCategory,
    cancelAddingCategory
} from '../../actions/categoryActions.js'


const mapStateToProps = (state) => {
    return {
        categories: state.categories
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: () => {
            dispatch(getFirstCategoriesPage())
        },
        onPreviousPageButtonClick: (categories) => {
            dispatch(getPreviousCategoriesPage(categories))
        },
        onNextPageButtonClick: (categories) => {
            dispatch(getNextCategoriesPage(categories))
        },
        onNewCategoryClick: () => {
            dispatch(showAddCategoryDialog())
        },
        onCategorySelection: (category) => {
            if (category) {
                dispatch(selectCategory(category))
            } else {
                dispatch(clearCategorySelection())
            }
        },
        onDeleteButtonClick: (category) => {
            dispatch(deleteCategory(category))
        },
        onEditButtonClick: () => {
            dispatch(startEditingSelectedCategory())
        },
        onSave: (originalCategory, updatedCategory) => {
            dispatch(updateCategory(originalCategory, updatedCategory))
        },
        onCancelEditing: () => {
            dispatch(cancelEditingCategory())
        },
        onCancelAdding: () => {
            dispatch(cancelAddingCategory())
        },
        onSaveNew: (category) => {
            dispatch(saveNewCategory(category))
        },
    }
}

const CategoriesContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Categories)

export default CategoriesContainer

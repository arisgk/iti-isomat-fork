import { connect } from 'react-redux'
import Products from '../../components/Products'
import {
    getFirstProductsPage,
    getPreviousProductsPage,
    getNextProductsPage,
    showAddProductDialog,
    selectProduct,
    clearProductSelection,
    deleteProduct,
    startEditingSelectedProduct,
    updateProduct,
    cancelEditingProduct,
    saveNewProduct,
    cancelAddingProduct
} from '../../actions/productActions.js'


const mapStateToProps = (state) => {
    return {
        products: state.products
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: () => {
            dispatch(getFirstProductsPage())
        },
        onPreviousPageButtonClick: (products) => {
            dispatch(getPreviousProductsPage(products))
        },
        onNextPageButtonClick: (products) => {
            dispatch(getNextProductsPage(products))
        },
        onNewProductClick: () => {
            dispatch(showAddProductDialog())
        },
        onProductSelection: (product) => {
            if (product) {
                dispatch(selectProduct(product))
            } else {
                dispatch(clearProductSelection())
            }
        },
        onDeleteButtonClick: (product) => {
            dispatch(deleteProduct(product))
        },
        onEditButtonClick: () => {
            dispatch(startEditingSelectedProduct())
        },
        onSave: (originalProduct, updatedProduct) => {
            dispatch(updateProduct(originalProduct, updatedProduct))
        },
        onCancelEditing: () => {
            dispatch(cancelEditingProduct())
        },
        onCancelAdding: () => {
            dispatch(cancelAddingProduct())
        },
        onSaveNew: (product) => {
            dispatch(saveNewProduct(product))
        },
    }
}

const ProductsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Products)

export default ProductsContainer

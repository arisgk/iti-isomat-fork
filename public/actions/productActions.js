import fetch from 'isomorphic-fetch'
require('es6-promise').polyfill()

function requestProducts() {
    return {
        type: 'REQUEST_PRODUCTS'
    }
}

function receiveProducts(json) {
    return {
        type: 'RECEIVE_PRODUCTS',
        products: json.data.products,
        page: json.data.page,
        pageCount: json.data.pageCount,
        itemCount: json.data.itemCount
    }
}

function invalidAction(cause) {
    return {
        type: 'INVALID_ACTION',
        cause: cause
    }
}

const getProductsPage = (page = 1, itemsPerPage = 50) => {

    return dispatch => {

        dispatch(requestProducts())

        return fetch(`/api/products?page=${page}&items=${itemsPerPage}`)
            .then(response => response.json())
            .then(json => dispatch(receiveProducts(json)))
    }
}

export function getFirstProductsPage () {
    return getProductsPage(1)
}

export function getPreviousProductsPage (products) {

    if (products && products.page > 1) {
        return getProductsPage(products.page - 1)
    }

    return invalidAction("There is no previous page")
}

export function getNextProductsPage (products) {

    if (products && products.page < products.pageCount) {
        return getProductsPage(products.page + 1)
    }

    return invalidAction("There is no next page")
}

export function showAddProductCard () {
    return {
        type: 'SHOW_ADD_PRODUCT_CARD'
    }
}

export function selectProduct (product) {
    return {
        type: 'SELECT_PRODUCT',
        product: product
    }
}

export function clearProductSelection () {
    return {
        type: 'CLEAR_PRODUCT_SELECTION'
    }
}

export function startEditingSelectedProduct () {
    return {
        type: 'START_EDITING_SELECTED_PRODUCT'
    }
}

export function cancelEditingProduct () {
    return {
        type: 'CANCEL_EDITING_PRODUCT'
    }
}

export function putProduct (updatedProduct) {
    return {
        type: 'PUT_PRODUCT',
        product: updatedProduct
    }
}

export function productUpdated (json, updatedProduct) {
    return {
        type: 'PRODUCT_UPDATED',
        product: updatedProduct
    }
}

function acceptUpdatedCategoriesIfValid (updatedProduct, originalProduct) {

    let catidArray = updatedProduct.catid.split(",").map(Number)
    if( Object.prototype.toString.call(catidArray) === '[object Array]' ) {
        return catidArray
    } else {
        return originalProduct.catid
    }
}

export function updateProduct (originalProduct, updatedProduct) {

    if (updatedProduct._id) {

        updatedProduct.catid = acceptUpdatedCategoriesIfValid(updatedProduct, originalProduct)

        return dispatch => {

            let product = {
                _id: updatedProduct._id,
                id: updatedProduct.id,
                title: updatedProduct.title,
                description: updatedProduct.description,
                catid: updatedProduct.catid,
                properties: updatedProduct.properties,
                shade: {
                    link: updatedProduct.shadeLink,
                    text: updatedProduct.shadeText
                },
                consumption: updatedProduct.consumption,
                package: updatedProduct.package,
                language: updatedProduct.language,
                pdf: updatedProduct.pdf,
                image: updatedProduct.image,
                video: updatedProduct.video
            }

            dispatch(putProduct(product))

            return fetch(`/api/products/${updatedProduct._id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(product)
            })
            .then(response => response.json())
            .then(json => dispatch(productUpdated(json, product)))
        }
    }

    return invalidAction("There is no product database ID")
}

export function deletingProduct () {
    return {
        type: 'DELETING_PRODUCT'
    }
}

export function productDeleted (json, product) {
    return {
        type: 'PRODUCT_DELETED',
        product: product
    }
}

export function deleteProduct (product) {

    if (product._id) {

        return dispatch => {

            dispatch(deletingProduct())

            return fetch(`/api/products/${product._id}`, {
                method: 'DELETE'
            })
            .then(response => response.json())
            .then(json => dispatch(productDeleted(json, product)))
        }
    }

    return invalidAction("There is no product database ID")
}

export function showAddProductDialog () {
    return {
        type: 'ADD_PRODUCT'
    }
}

export function cancelAddingProduct () {
    return {
        type: 'CANCEL_ADDING_PRODUCT'
    }
}

export function postProduct (product) {
    return {
        type: 'POST_PRODUCT',
        product: product
    }
}

export function productAdded (json, product) {
    return {
        type: 'PRODUCT_ADDED',
        product: product
    }
}

function acceptCategoriesIfValid (product) {

    let catidArray = product.catid.split(",").map(Number)
    if( Object.prototype.toString.call(catidArray) === '[object Array]' ) {
        return catidArray
    } else {
        return []
    }
}

export function saveNewProduct (newProduct) {

    if (newProduct.id) {

        newProduct.catid = acceptCategoriesIfValid(newProduct)

        return dispatch => {

            let product = {
                id: newProduct.id,
                title: newProduct.title,
                description: newProduct.description,
                catid: newProduct.catid,
                properties: newProduct.properties,
                shade: {
                    link: newProduct.shadeLink,
                    text: newProduct.shadeText
                },
                consumption: newProduct.consumption,
                package: newProduct.package,
                language: newProduct.language,
                pdf: newProduct.pdf,
                image: newProduct.image,
                video: newProduct.video
            }

            dispatch(postProduct(product))

            return fetch(`/api/products`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(product)
            })
            .then(response => response.json())
            .then(json => dispatch(productAdded(json, product)))
        }
    }

    return invalidAction("There is no product database ID")
}

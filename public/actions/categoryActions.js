import fetch from 'isomorphic-fetch'
require('es6-promise').polyfill()

function requestCategories() {
    return {
        type: 'REQUEST_CATEGORIES'
    }
}

function receiveCategories(json) {
    return {
        type: 'RECEIVE_CATEGORIES',
        categories: json.data.categories,
        page: json.data.page,
        pageCount: json.data.pageCount,
        itemCount: json.data.itemCount
    }
}

function invalidAction(cause) {
    return {
        type: 'INVALID_ACTION',
        cause: cause
    }
}

const getCategoriesPage = (page = 1, itemsPerPage = 50) => {

    return dispatch => {

        dispatch(requestCategories())

        return fetch(`/api/categories?page=${page}&items=${itemsPerPage}`)
            .then(response => response.json())
            .then(json => dispatch(receiveCategories(json)))
    }
}

export function getFirstCategoriesPage () {
    return getCategoriesPage(1)
}

export function getPreviousCategoriesPage (categories) {

    if (categories && categories.page > 1) {
        return getCategoriesPage(categories.page - 1)
    }

    return invalidAction("There is no previous page")
}

export function getNextCategoriesPage (categories) {

    if (categories && categories.page < categories.pageCount) {
        return getCategoriesPage(categories.page + 1)
    }

    return invalidAction("There is no next page")
}

export function showAddCategoryCard () {
    return {
        type: 'SHOW_ADD_CATEGORY_CARD'
    }
}

export function selectCategory (category) {
    return {
        type: 'SELECT_CATEGORY',
        category: category
    }
}

export function clearCategorySelection () {
    return {
        type: 'CLEAR_CATEGORY_SELECTION'
    }
}

export function startEditingSelectedCategory () {
    return {
        type: 'START_EDITING_SELECTED_CATEGORY'
    }
}

export function cancelEditingCategory () {
    return {
        type: 'CANCEL_EDITING_CATEGORY'
    }
}

export function putCategory (updatedCategory) {
    return {
        type: 'PUT_CATEGORY',
        category: updatedCategory
    }
}

export function categoryUpdated (json, updatedCategory) {
    return {
        type: 'CATEGORY_UPDATED',
        category: updatedCategory
    }
}

export function updateCategory (originalCategory, updatedCategory) {

    console.log(updatedCategory)

    if (updatedCategory._id) {

        return dispatch => {

            let category = {
                _id: updatedCategory._id,
                id: updatedCategory.id,
                title: updatedCategory.title,
                parentid: updatedCategory.parentid,
                color: updatedCategory.color,
                bgcolor: updatedCategory.bgcolor,
                language: updatedCategory.language,
                children: updatedCategory.children,
                products: updatedCategory.products,
                ordering: updatedCategory.ordering,
            }

            dispatch(putCategory(category))

            return fetch(`/api/categories/${updatedCategory._id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(category)
            })
            .then(response => response.json())
            .then(json => dispatch(categoryUpdated(json, category)))
        }
    }

    return invalidAction("There is no category database ID")
}

export function deletingCategory () {
    return {
        type: 'DELETING_CATEGORY'
    }
}

export function categoryDeleted (json, category) {
    return {
        type: 'CATEGORY_DELETED',
        category: category
    }
}

export function deleteCategory (category) {

    if (category._id) {

        return dispatch => {

            dispatch(deletingCategory())

            return fetch(`/api/categories/${category._id}`, {
                method: 'DELETE'
            })
            .then(response => response.json())
            .then(json => dispatch(categoryDeleted(json, category)))
        }
    }

    return invalidAction("There is no category database ID")
}

export function showAddCategoryDialog () {
    return {
        type: 'ADD_CATEGORY'
    }
}

export function cancelAddingCategory () {
    return {
        type: 'CANCEL_ADDING_CATEGORY'
    }
}

export function postCategory (category) {
    return {
        type: 'POST_CATEGORY',
        category: category
    }
}

export function categoryAdded (json, category) {
    return {
        type: 'CATEGORY_ADDED',
        category: category
    }
}

export function saveNewCategory (newCategory) {

    if (newCategory.id) {

        return dispatch => {

            let category = {
                id: newCategory.id,
                title: newCategory.title,
                parentid: newCategory.parentid,
                color: newCategory.color,
                bgcolor: newCategory.bgcolor,
                language: newCategory.language,
                children: newCategory.children,
                products: newCategory.products,
                ordering: newCategory.ordering,
            }

            dispatch(postCategory(category))

            return fetch(`/api/categories`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(category)
            })
            .then(response => response.json())
            .then(json => dispatch(categoryAdded(json, category)))
        }
    }

    return invalidAction("There is no category database ID")
}

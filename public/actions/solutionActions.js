import fetch from 'isomorphic-fetch'
require('es6-promise').polyfill()

function invalidAction(cause) {
    return {
        type: 'INVALID_ACTION',
        cause: cause
    }
}

function requestSolutions() {
    return {
        type: 'REQUEST_SOLUTIONS'
    }
}

function receiveSolutions(json) {
    return {
        type: 'RECEIVE_SOLUTIONS',
        solutions: json.data.solutions,
    }
}

export function getSolutions () {

    return dispatch => {

        dispatch(requestSolutions())

        return fetch(`/api/solutions`)
            .then(response => response.json())
            .then(json => dispatch(receiveSolutions(json)))
    }
}

function requestSolution() {
    return {
        type: 'REQUEST_SOLUTION'
    }
}

function receiveSolution(json) {
    return {
        type: 'RECEIVE_SOLUTION',
        solution: json.data.solution,
    }
}

export function getSolutionFromAPI (id) {

    return dispatch => {

        dispatch(requestSolution())

        return fetch(`/api/solutions/${id}`)
            .then(response => response.json())
            .then(json => dispatch(receiveSolution(json)))
    }
}

export function putSolution (updatedSolution) {
    return {
        type: 'PUT_SOLUTION',
        solution: updatedSolution
    }
}

export function solutionUpdated (json, updatedSolution) {
    return {
        type: 'SOLUTION_UPDATED',
        solution: updatedSolution
    }
}

export function updateSolution (originalSolution, updatedSolution) {

    if (updatedSolution._id) {

        return dispatch => {

            let solution = {
                _id: updatedSolution._id,
                id: updatedSolution.id,
                title: updatedSolution.title,
                description: updatedSolution.description,
                images: updatedSolution.images,
                video: updatedSolution.video
            }

            dispatch(putSolution(solution))

            return fetch(`/api/solutions/${updatedSolution._id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(solution)
            })
            .then(response => response.json())
            .then(json => dispatch(solutionUpdated(json, solution)))
        }
    }

    return invalidAction("There is no solution database ID")
}

export function postSolution (solution) {
    return {
        type: 'POST_SOLUTION',
        solution
    }
}

export function solutionAdded (json, solution, router) {
    router.push(`/solutions`)
    return {
        type: 'SOLUTION_ADDED',
        solution,
    }
}

export function saveNewSolution (newSolution, router) {

    if (newSolution.id) {

        return dispatch => {

            let solution = {
                id: newSolution.id,
                title: newSolution.title,
                description: newSolution.description,
                video: newSolution.video,
                images: newSolution.images
            }

            dispatch(postSolution(solution))

            return fetch(`/api/solutions`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(solution)
            })
            .then(response => response.json())
            .then(json => dispatch(solutionAdded(json, solution, router)))
        }
    }

    return invalidAction("There is no solution database ID")
}

function postSolutionImage (solution) {
    return {
        type: 'POST_SOLUTION_IMAGE',
        solution
    }
}

function solutionImageUploaded (json) {
    return {
        type: 'SOLUTION_IMAGE_UPLOADED',
        solution: json.data.solution
    }
}

export function uploadImage (file, solution) {

    return dispatch => {

        dispatch(postSolutionImage(solution))

        const data = new FormData()
        data.append('file', file)

        return fetch(`/api/solutions/${solution._id}/images`, {
            method: 'POST',
            body: data
        })
        .then(response => response.json())
        .then(json => dispatch(solutionImageUploaded(json)))
    }
}

function deletingSolutionImage (solution) {
    return {
        type: 'DELETING_SOLUTION_IMAGE',
        solution
    }
}

function solutionImageDeleted (json) {
    console.log(json.data.solution)
    return {
        type: 'SOLUTION_IMAGE_DELETED',
        solution: json.data.solution
    }
}

export function deleteImage (image, solution) {

    return dispatch => {

        dispatch(deletingSolutionImage(solution))

        return fetch(`/api/solutions/${solution._id}/images?image=${image}`, {
            method: 'DELETE'
        })
        .then(response => response.json())
        .then(json => dispatch(solutionImageDeleted(json)))
    }
}

import fetch from 'isomorphic-fetch'
import $ from 'jquery'
require('es6-promise').polyfill()
import { getFirstCategoriesPage } from './categoryActions'
import { getFirstProductsPage } from './productActions'

function startImporting () {
    return {
        type: 'START_IMPORTING'
    }
}

function completedImporting (json) {
    return {
        type: 'COMPLETED_IMPORTING'
    }
}

export function importData () {
    return dispatch => {

        dispatch(startImporting())

        var xhr = new XMLHttpRequest()
        xhr.open('GET', `/api/import`, true)

        xhr.timeout = 3600000

        xhr.onload = function () {
            dispatch(completedImporting())
        }

        xhr.ontimeout = function (e) {
            console.log('Import request timeout')
        }

        xhr.send(null)
    }
}

export function startExporting () {
    return {
        type: 'START_EXPORTING'
    }
}

export function completedExporting () {
    return {
        type: 'COMPLETED_EXPORTING'
    }
}

function invalidAction(cause) {
    return {
        type: 'INVALID_ACTION',
        cause: cause
    }
}

function searching (collection) {

    switch (collection) {
        case 'categories':
            return {
                type: 'SEARCHING_CATEGORIES'
            }
        case 'products':
            return {
                type: 'SEARCHING_PRODUCTS'
            }
        default:
            return invalidAction('Invalid search query')
    }


}

function searchCompleted (json, collection) {

    switch (collection) {
        case 'categories':
            return {
                type: 'COMPLETED_SEARCHING_CATEGORIES',
                categories: json.data.categories,
            }
        case 'products':
            return {
                type: 'COMPLETED_SEARCHING_PRODUCTS',
                products: json.data.products,
            }
        default:
            return invalidAction('Invalid search query')
    }

    return {
        type: 'COMPLETED_SEARCHING',
    }
}

export function search (string, collection) {

    if (string) {
        return dispatch => {

            dispatch(searching(collection))

            return fetch(`/api/${collection}?searchText=${string}`, {
                method: 'GET'
            })
            .then(response => response.json())
            .then(json => dispatch(searchCompleted(json, collection)))
        }
    }

    return invalidAction("There is no category database ID")
}

export function resetSearch (collection) {

    switch (collection) {
        case 'categories':
            return getFirstCategoriesPage()
        case 'products':
            return getFirstProductsPage()
        default:
            return invalidAction()
    }
}

function requestSearchSuggestions(input) {
    return {
        type: 'REQUEST_SEARCH_SUGGESTIONS',
        input: input,
    }
}

function fetchedSearchSuggestions(json, collection, input) {

    switch (collection) {
        case 'categories':
            return {
                type: 'FETCHED_SEARCH_SUGGESTIONS',
                suggestions: json.data.categories.map(category => category.title),
                suggestionsInput: input,
            }
        case 'products':
            return {
                type: 'FETCHED_SEARCH_SUGGESTIONS',
                suggestions: json.data.products.map(product => product.title),
                suggestionsInput: input,
            }
        default:
            return invalidAction('Invalid search query')
    }
}

export function getSearchSuggestions (string, collection) {

    if (string) {
        return dispatch => {

            dispatch(requestSearchSuggestions(string))

            return fetch(`/api/${collection}?searchText=${string}`, {
                method: 'GET'
            })
            .then(response => response.json())
            .then(json => dispatch(fetchedSearchSuggestions(json, collection, string)))
        }
    }

    return invalidAction("There is no category database ID")
}

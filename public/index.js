// Importing React Library
import React from 'react'
import { render } from 'react-dom'

// Importing React Components
import Root from './components/Root'

// Importing React Redux
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from './reducers'

// Importing React tap event plugin (required by Material-UI)
import injectTapEventPlugin from 'react-tap-event-plugin'

//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin()

// Create the Redux store
let store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware, // lets us dispatch() functions
    )
)

// Instantiate the root component
render(
    <Provider store={store}>
        <Root />
    </Provider>,
    document.getElementById('root')
)

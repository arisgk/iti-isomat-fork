let initialState = {
    isFetchingSolutions: false,
    isAddingSolution: false,
    isUpdatingSolution: false,
    data: []
}

const solution = (state, action) => {
    switch (action.type) {
        case 'RECEIVE_SOLUTION':
            if (action.solution._id === state._id) {
                return action.solution
            } else {
                return state
            }
        case 'SOLUTION_UPDATED':
        case 'SOLUTION_IMAGE_UPLOADED':
        case 'SOLUTION_IMAGE_DELETED':
            if (action.solution._id === state._id) {
                return action.solution
            }

            return state
        default:
            return state
    }
}

const solutions = (state = initialState, action) => {

    switch (action.type) {
        case 'REQUEST_SOLUTIONS':
            return Object.assign({}, state, {
                isFetchingSolutions: true
            })
        case 'RECEIVE_SOLUTIONS':
            return Object.assign({}, state, {
                isFetchingSolutions: false,
                data: action.solutions,
            })
        case 'REQUEST_SOLUTION':
            return Object.assign({}, state, {
                isUpdatingSolution: true
            })
        case 'RECEIVE_SOLUTION':
            return Object.assign({}, state, {
                isUpdatingSolution: false,
                data: state.data.concat(action.solution),
            })
        case 'PUT_SOLUTION':
        case 'POST_SOLUTION_IMAGE':
        case 'DELETING_SOLUTION_IMAGE':
            return Object.assign({}, state, {
                isUpdatingSolution: true,
            })
        case 'SOLUTION_UPDATED':
        case 'SOLUTION_IMAGE_UPLOADED':
        case 'SOLUTION_IMAGE_DELETED':
            return Object.assign({}, state, {
                isUpdatingSolution: false,
                data: state.data.map(sol => solution(sol, action))
            })
        case 'POST_SOLUTION':
            return Object.assign({}, state, {
                isAddingSolution: true
            })
        case 'SOLUTION_ADDED':
            return Object.assign({}, state, {
                isAddingSolution: false,
                data: state.data.concat(action.solution)
            })
        default:
            return state
    }
}

export default solutions

import { combineReducers } from 'redux'
import appBar from './appBar'
import categories from './categories'
import products from './products'
import solutions from './solutions'

const rootReducer = combineReducers({
    appBar,
    categories,
    products,
    solutions,
})

export default rootReducer

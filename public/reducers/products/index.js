let initialState = {
    isFetchingProducts: false,
    isAddingProduct: false,
    isUpdatingProduct: false,
    data: []
}

const product = (state, action) => {

    switch (action.type) {
        case 'SELECT_PRODUCT':
            if (action.product.id !== state.id) {
                return Object.assign({}, state, { selected: false })
            }

            return Object.assign({}, state, { selected: true })
        case 'CLEAR_PRODUCT_SELECTION':
            return Object.assign({}, state, { selected: false })
        case 'START_EDITING_SELECTED_PRODUCT':
            if (!state.selected) {
                return state
            }

            return Object.assign({}, state, { isEditing: true })
        case 'CANCEL_EDITING_PRODUCT':
            return Object.assign({}, state, { isEditing: false})
        case 'PUT_PRODUCT':
            if (action.product.id === state.id) {
                return Object.assign({}, action.product, { isEditing: true })
            }

            return state
        case 'PRODUCT_UPDATED':
            if (action.product.id === state.id) {
                return Object.assign({}, action.product, { isEditing: false })
            }

            return state
        case 'PRODUCT_DELETED':
            return action.product.id !== state.id
        default:
            return state
    }
}

const products = (state = initialState, action) => {

    switch (action.type) {
        case 'REQUEST_PRODUCTS':
        case 'SEARCHING_PRODUCTS':
            return Object.assign({}, state, {
                isFetchingProducts: true
            })
        case 'RECEIVE_PRODUCTS':
            return Object.assign({}, state, {
                isFetchingProducts: false,
                data: action.products,
                page: action.page,
                pageCount: action.pageCount,
                itemCount: action.itemCount
            })
        case 'SHOW_ADD_PRODUCT_CARD':
            return Object.assign({}, state, {
                isAddingProduct: true
            })
        case 'SELECT_PRODUCT':
            return Object.assign({}, state, {
                data: state.data.map(prod => product(prod, action))
            })
        case 'CLEAR_PRODUCT_SELECTION':
            return Object.assign({}, state, {
                data: state.data.map(prod => product(prod, action))
            })
        case 'START_EDITING_SELECTED_PRODUCT':
            return Object.assign({}, state, {
                data: state.data.map(prod => product(prod, action))
            })
        case 'CANCEL_EDITING_PRODUCT':
            return Object.assign({}, state, {
                data: state.data.map(prod => product(prod, action))
            })
        case 'PUT_PRODUCT':
            return Object.assign({}, state, {
                isUpdatingProduct: true,
                data: state.data.map(prod => product(prod, action))
            })
        case 'PRODUCT_UPDATED':
            return Object.assign({}, state, {
                isUpdatingProduct: false,
                data: state.data.map(prod => product(prod, action))
            })
        case 'DELETING_PRODUCT':
            return Object.assign({}, state, {
                isFetchingProducts: true
            })
        case 'PRODUCT_DELETED':
            return Object.assign({}, state, {
                isFetchingProducts: false,
                data: state.data.filter(prod => product(prod, action))
            })
        case 'ADD_PRODUCT':
            return Object.assign({}, state, {
                isAddingProduct: true
            })
        case 'CANCEL_ADDING_PRODUCT':
            return Object.assign({}, state, {
                isAddingProduct: false
            })
        case 'POST_PRODUCT':
            return Object.assign({}, state, {
                isUpdatingProduct: true
            })
        case 'PRODUCT_ADDED':
            return Object.assign({}, state, {
                isUpdatingProduct: false,
                isAddingProduct: false,
                data: state.data.concat(action.product)
            })
        case 'COMPLETED_SEARCHING_PRODUCTS':
            return Object.assign({}, state, {
                isFetchingProducts: false,
                data: action.products,
            })
        default:
            return state
    }
}

export default products

let initialState = {
    importing: false,
    exporting: false,
    searchSuggestions: [],
    latestSearchInput: ''
}

const appBar = (state = initialState, action) => {
    switch (action.type) {
        case 'START_IMPORTING':
            return Object.assign({}, state, { importing: true })
        case 'COMPLETED_IMPORTING':
            return Object.assign({}, state, { importing: false })
        case 'START_EXPORTING':
            return Object.assign({}, state, { exporting: true })
        case 'COMPLETED_EXPORTING':
            return Object.assign({}, state, { exporting: false })
        case 'SEARCHING':
            return Object.assign({}, state, { searching: true })
        case 'REQUEST_SEARCH_SUGGESTIONS':
            return Object.assign({}, state, { latestSearchInput: action.input })
        case 'FETCHED_SEARCH_SUGGESTIONS':
            if (action.suggestionsInput.toUpperCase() === state.latestSearchInput.toUpperCase()) {
                return Object.assign({}, state, { searchSuggestions: action.suggestions })
            }
            return state
        default:
            return state
    }
}

export default appBar

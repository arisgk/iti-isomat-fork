let initialState = {
    isFetchingCategories: false,
    isAddingCategory: false,
    isUpdatingCategory: false,
    data: []
}

const category = (state, action) => {

    switch (action.type) {
        case 'SELECT_CATEGORY':
            if (action.category.id !== state.id) {
                return Object.assign({}, state, { selected: false })
            }

            return Object.assign({}, state, { selected: true })
        case 'CLEAR_CATEGORY_SELECTION':
            return Object.assign({}, state, { selected: false })
        case 'START_EDITING_SELECTED_CATEGORY':
            if (!state.selected) {
                return state
            }

            return Object.assign({}, state, { isEditing: true })
        case 'CANCEL_EDITING_CATEGORY':
            return Object.assign({}, state, { isEditing: false})
        case 'PUT_CATEGORY':
            if (action.category.id === state.id) {
                return Object.assign({}, action.category, { isEditing: true })
            }

            return state
        case 'CATEGORY_UPDATED':
            if (action.category.id === state.id) {
                return Object.assign({}, action.category, { isEditing: false })
            }

            return state
        case 'CATEGORY_DELETED':
            return action.category.id !== state.id
        default:
            return state
    }
}

const categories = (state = initialState, action) => {

    switch (action.type) {
        case 'REQUEST_CATEGORIES':
        case 'SEARCHING_CATEGORIES':
            return Object.assign({}, state, {
                isFetchingCategories: true
            })
        case 'RECEIVE_CATEGORIES':
            return Object.assign({}, state, {
                isFetchingCategories: false,
                data: action.categories,
                page: action.page,
                pageCount: action.pageCount,
                itemCount: action.itemCount
            })
        case 'SHOW_ADD_CATEGORY_CARD':
            return Object.assign({}, state, {
                isAddingCategory: true
            })
        case 'SELECT_CATEGORY':
            return Object.assign({}, state, {
                data: state.data.map(prod => category(prod, action))
            })
        case 'CLEAR_CATEGORY_SELECTION':
            return Object.assign({}, state, {
                data: state.data.map(prod => category(prod, action))
            })
        case 'START_EDITING_SELECTED_CATEGORY':
            return Object.assign({}, state, {
                data: state.data.map(prod => category(prod, action))
            })
        case 'CANCEL_EDITING_CATEGORY':
            return Object.assign({}, state, {
                data: state.data.map(prod => category(prod, action))
            })
        case 'PUT_CATEGORY':
            return Object.assign({}, state, {
                isUpdatingCategory: true,
                data: state.data.map(prod => category(prod, action))
            })
        case 'CATEGORY_UPDATED':
            return Object.assign({}, state, {
                isUpdatingCategory: false,
                data: state.data.map(prod => category(prod, action))
            })
        case 'DELETING_CATEGORY':
            return Object.assign({}, state, {
                isFetchingCategories: true
            })
        case 'CATEGORY_DELETED':
            return Object.assign({}, state, {
                isFetchingCategories: false,
                data: state.data.filter(prod => category(prod, action))
            })
        case 'ADD_CATEGORY':
            return Object.assign({}, state, {
                isAddingCategory: true
            })
        case 'CANCEL_ADDING_CATEGORY':
            return Object.assign({}, state, {
                isAddingCategory: false
            })
        case 'POST_CATEGORY':
            return Object.assign({}, state, {
                isUpdatingCategory: true
            })
        case 'CATEGORY_ADDED':
            return Object.assign({}, state, {
                isUpdatingCategory: false,
                isAddingCategory: false,
                data: state.data.concat(action.category)
            })
        case 'COMPLETED_SEARCHING_CATEGORIES':
            return Object.assign({}, state, {
                isFetchingCategories: false,
                data: action.categories,
            })
        default:
            return state
    }
}

export default categories

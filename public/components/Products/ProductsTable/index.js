import React from 'react'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter } from 'material-ui/Table'
import IconButton from 'material-ui/IconButton'
import TableFooterControls from './TableFooterControls'
import CircularProgress from 'material-ui/CircularProgress'
import $ from 'jquery'

const styles = {
	iconButton: {
	    width: '32px',
	    height: '32px',
	    padding: 0
	},
	tableFooterRow: {
        textAlign: 'right'
    }
}

// Products table presentational component. Based on Material-UI table.
class ProductsTable extends React.Component {

    componentDidMount () {

        let { fetchData } = this.props
        fetchData()
    }

	calculateTableHeight () {
		const appBarHeight = 64
		const tableTitleHeight = 64
		const tableHeaderHeight = 57
		const tableFooterHeight = 48

		return window.innerHeight - (appBarHeight + tableTitleHeight + tableHeaderHeight + tableFooterHeight + 8)
	}

	getSelectedProduct (products) {
		if (products) {
			for (let product of products) {
				if (product.selected) return product
			}
		}

		return null
	}

    render () {

        let { products, onPreviousPageButtonClick, onNextPageButtonClick, onProductSelection, onEditButtonClick, onDeleteButtonClick } = this.props

        let data = products.data
		let isFetchingProducts = products.isFetchingProducts

		let tableHeight = `${this.calculateTableHeight()}px`
		let selectedProduct = this.getSelectedProduct(data)

        return (
            <div className="products-table-container">

                <div className="table-title-container">
					<div className="title-and-spinner">
						<h2>Products</h2>
						<CircularProgress size={0.4} className={isFetchingProducts ? "visible" : "visibility-hidden"} />
					</div>
					<IconButton className="delete-button" style={styles.iconButton} disabled={!selectedProduct} onTouchTap={() => onDeleteButtonClick(selectedProduct)}>
    					<i className="material-icons">delete</i>
    				</IconButton>
                    <IconButton className="edit-button" style={styles.iconButton} disabled={!selectedProduct} onTouchTap={() => onEditButtonClick()}>
    					<i className="material-icons">mode_edit</i>
    				</IconButton>
                </div>

                <Table fixedHeader={true} fixedFooter={true} height={tableHeight} onRowSelection={(selectedRows) => onProductSelection(data[selectedRows[0]])}>
                    <TableHeader>
                        <TableRow>
                            <TableHeaderColumn>ID</TableHeaderColumn>
                            <TableHeaderColumn>Title</TableHeaderColumn>
                            <TableHeaderColumn>Package</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        {data.map(product =>
                            <TableRow key={product.id} selected={product.selected}>
                                <TableRowColumn>{product.id}</TableRowColumn>
                                <TableRowColumn>{product.title}</TableRowColumn>
                                <TableRowColumn>{product.package}</TableRowColumn>
                            </TableRow>
                        )}
                    </TableBody>
					<TableFooter style={styles.tableFooter}>
	                    <TableRow style={styles.tableFooterRow}>
	                        <TableFooterControls
								products={products}
	                            onPreviousPageClick={() => onPreviousPageButtonClick(products)}
	                            onNextPageClick={() => onNextPageButtonClick(products)}
	                        />
	                    </TableRow>
                	</TableFooter>
                </Table>
            </div>
        )
    }
}

export default ProductsTable

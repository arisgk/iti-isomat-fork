import React from 'react'

const style = {
	color: 'rgba(0,0,0,0.54)',
	fontWeight: 700
}

const pageItems = 50

const ItemsCountLabel = ({ products }) => {

	if (products && products.data && products.page && products.itemCount) {

		const startIndex = ((products.page - 1) * pageItems) + 1
		const endIndex = startIndex + (products.data.length - 1)
		const itemCount = products.itemCount

		return (
			<span style={style}>
				{`${startIndex}-${endIndex} of ${itemCount}`}
			</span>
		)
	} else {
		return (
			<span></span>
		)
	}
}


export default ItemsCountLabel

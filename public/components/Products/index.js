import React from 'react'
import ProductsTable from './ProductsTable'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import EditProductDialog from './EditProductDialog'
import AddProductDialog from './AddProductDialog'

const styles = {
    fab: {
        position: 'fixed',
        bottom: 64,
        right: 24
    }
}

class ProductsTab extends React.Component {

    getEditingProduct (products) {
        if (products) {
            for (let product of products) {
                if (product.isEditing) return product
            }
        }

        return null
    }

    render () {

        let {
            products,
            fetchData,
            onPreviousPageButtonClick,
            onNextPageButtonClick,
            onNewProductClick,
            onProductSelection,
            onDeleteButtonClick,
            onEditButtonClick,
            onSave,
            onCancelEditing,
            onSaveNew,
            onCancelAdding
        } = this.props

        return (
            <div className="products-tab-container">

                <ProductsTable
                    products={products}
                    onPreviousPageButtonClick={onPreviousPageButtonClick}
                    onNextPageButtonClick={onNextPageButtonClick}
                    onProductSelection={onProductSelection}
                    fetchData={fetchData}
                    onEditButtonClick={onEditButtonClick}
                    onDeleteButtonClick={onDeleteButtonClick}
                />

                <FloatingActionButton style={styles.fab} onTouchTap={() => onNewProductClick()} >
                    <ContentAdd />
                </FloatingActionButton>

                <EditProductDialog product={this.getEditingProduct(products.data)} isUpdatingProduct={products.isUpdatingProduct} onSave={onSave} onCancelEditing={onCancelEditing} />
                <AddProductDialog isAddingProduct={products.isAddingProduct} isUpdatingProduct={products.isUpdatingProduct} onSaveNew={onSaveNew} onCancelAdding={onCancelAdding} />
            </div>
        )
    }

}

export default ProductsTab

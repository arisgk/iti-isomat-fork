import React, {Component} from 'react'
import Dialog from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import CircularProgress from 'material-ui/CircularProgress'

const styles = {
    container: {
        maxWidth: 'none',
        width: '60%'
    },
    spinner: {
        position: 'absolute',
        top: 16,
        right: 24
    }
}

class AddProductDialog extends Component {

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        })
    }

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            title: '',
            description: '',
            properties: '',
            consumption: '',
            package: '',
            language: '',
            catid: '',
            shadeLink: '',
            shadeText: '',
            pdf: '',
            image: '',
            video: '',
            deleted: false
        }
    }

    render() {

        let {
            onCancelAdding,
            onSaveNew,
            isAddingProduct,
            isUpdatingProduct
        } = this.props

        const actions = [
            <FlatButton
                label="Cancel"
                onTouchTap={() => onCancelAdding()}
            />,
            <FlatButton
                label="Save"
                primary={true}
                onTouchTap={() => onSaveNew(this.state)}
            />
        ]

        return (
            <Dialog
                title={"New Product"}
                actions={actions}
                modal={false}
                contentStyle={styles.container}
                open={isAddingProduct ? true : false}
                onRequestClose={() => onCancelAdding()}
                autoScrollBodyContent={true}
            >
                <CircularProgress size={0.4} className={isUpdatingProduct ? "visible" : "visibility-hidden"} style={styles.spinner} />

                <TextField name="id" floatingLabelText={"ID"} floatingLabelFixed={true} value={this.state.id} onChange={this.handleChange} fullWidth={true}     />
                <TextField name="title" floatingLabelText={"Title"} floatingLabelFixed={true} value={this.state.title} onChange={this.handleChange} fullWidth={true} />
                <TextField name="description" multiLine={true} floatingLabelText={"Description"} floatingLabelFixed={true} value={this.state.description} onChange={this.handleChange} fullWidth={true} />
                <TextField name="catid" floatingLabelText={"Categories"} floatingLabelFixed={true} value={this.state.catid} onChange={this.handleChange} fullWidth={true} />
                <TextField name="properties" multiLine={true} floatingLabelText={"Properties"} floatingLabelFixed={true} value={this.state.properties} onChange={this.handleChange} fullWidth={true} />
                <TextField name="consumption" multiLine={true} floatingLabelText={"Consumption"} floatingLabelFixed={true} value={this.state.consumption} onChange={this.handleChange} fullWidth={true} />
                <TextField name="package" floatingLabelText={"Package"} floatingLabelFixed={true} value={this.state.package} onChange={this.handleChange} fullWidth={true} />
                <TextField name="language" floatingLabelText={"Language"} floatingLabelFixed={true} value={this.state.language} onChange={this.handleChange} fullWidth={true} />
                <TextField name="shadeLink" floatingLabelText={"Shade Link"} floatingLabelFixed={true} value={this.state.shadeLink} onChange={this.handleChange} fullWidth={true} />
                <TextField name="shadeText" floatingLabelText={"Shade Text"} floatingLabelFixed={true} value={this.state.shadeText} onChange={this.handleChange} fullWidth={true} />
                <TextField name="pdf" floatingLabelText={"PDF URL"} floatingLabelFixed={true} value={this.state.pdf} onChange={this.handleChange} fullWidth={true} />
                <TextField name="image" floatingLabelText={"Image URL"} floatingLabelFixed={true} value={this.state.image} onChange={this.handleChange} fullWidth={true} />
                <TextField name="video" floatingLabelText={"Video URL"} floatingLabelFixed={true} value={this.state.video} onChange={this.handleChange} fullWidth={true} />
            </Dialog>
        )
    }
}

export default AddProductDialog

import React from 'react'
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import CircularProgress from 'material-ui/CircularProgress'
import FileDownload from '../FileDownload'
import SearchField from './SearchField'

const styles = {
    appBar: {
        boxShadow: 'none'
    },
    iconButton: {
	    width: '64px',
	    height: '64px'
	},
    spinner: {
        alignSelf: 'center'
    }
}

const LogoTitleAppBar = ({ importing, exporting, searchSuggestions, onImportClick, onExportClick, onDownloadComplete, onSearch, onSearchReset, onSearchTyping, ...props }) => {
    console.log(searchSuggestions);
    if (exporting) {
        // Add FileDownload component to app bar to download exported data
        return (
            <AppBar {...props} className="app-bar" title="" style={styles.appBar}>

                <SearchField suggestions={searchSuggestions} onSearch={onSearch} onSearchReset={onSearchReset} onSearchTyping={onSearchTyping} />

                <CircularProgress style={styles.spinner} size={0.4} color="white" className={(importing || exporting || searching) ? "visible" : "visibility-hidden"} />

                <IconButton className="app-bar-icon-button" tooltip="Import" tooltipPosition="bottom-right" style={styles.iconButton} onTouchTap={() => onImportClick()}>
                    <i className="material-icons">file_upload</i>
                </IconButton>
                <IconButton className="app-bar-icon-button" tooltip="Export" tooltipPosition="bottom-right" style={styles.iconButton} onTouchTap={() => onExportClick()}>
                    <i className="material-icons">file_download</i>
                </IconButton>

                <FileDownload actionPath={'/api/export'} method={'GET'} onDownloadComplete={onDownloadComplete}/>
            </AppBar>
        )
    } else {
        return (
            <AppBar {...props} className="app-bar" title="" style={styles.appBar}>

                <SearchField suggestions={searchSuggestions} onSearch={onSearch} onSearchReset={onSearchReset} onSearchTyping={onSearchTyping}/>

                <CircularProgress style={styles.spinner} size={0.4} color="white" className={(importing || exporting) ? "visible" : "visibility-hidden"} />

                <IconButton className="app-bar-icon-button" tooltip="Import" tooltipPosition="bottom-right" style={styles.iconButton} onTouchTap={() => onImportClick()}>
                    <i className="material-icons">file_upload</i>
                </IconButton>
                <IconButton className="app-bar-icon-button" tooltip="Export" tooltipPosition="bottom-right" style={styles.iconButton} onTouchTap={() => onExportClick()}>
                    <i className="material-icons">file_download</i>
                </IconButton>
            </AppBar>
        )
    }
}

export default LogoTitleAppBar

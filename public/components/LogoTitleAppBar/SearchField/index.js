import React from 'react';
import AutoComplete from 'material-ui/AutoComplete'

const styles = {
    autocomplete: {
        paddingLeft: 6,
        color: 'rgba(255, 255, 255, 0.87)',
    },
    input: {
        color: 'rgba(255, 255, 255, 0.87)',
        fontSize: '100%',
        fontWeight: 300,
        maxWidth: 144,
    },
    hint: {
        color: 'rgba(255, 255, 255, 0.54)',
    }
}

export default class SearchField extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            value: '',
            dataSource: [],
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleRequestClose = this.handleRequestClose.bind(this);
        this.handleUpdateInput = this.handleUpdateInput.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleRequestClose() {
        this.setState({open: false});
    }

    handleUpdateInput(value) {
        const { onSearchReset, onSearchTyping } = this.props;

        if (!value) {
            onSearchReset()
        }

        onSearchTyping(value)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.suggestions) {
            this.setState({ ...this.state, dataSource: nextProps.suggestions })
        }
    }

    render() {
        const { onSearch } = this.props;

        /**
         * The root div of the component is referenced in order to be passed to the popover.
         * -- https://facebook.github.io/react/docs/more-about-refs.html
        */
        return (
            <div className="search-field" ref={(rootDiv) => { this.rootDiv = rootDiv; }}>
                <i className="material-icons">search</i>
                <div className="input-container">
                    <AutoComplete
                        hintText="Search"
                        underlineShow={false}
                        fullWidth={false}
                        style={styles.autocomplete}
                        inputStyle={styles.input}
                        hintStyle={styles.hint}
                        menuStyle={styles.menuStyle}
                        dataSource={this.state.dataSource}
                        onUpdateInput={this.handleUpdateInput}
                        onNewRequest={(chosenRequest) => onSearch(chosenRequest)}
                        maxSearchResults={4}
                        filter={(searchText, key) => searchText !== ''}
                    />
                </div>
            </div>
        );
        // <input placeholder="Search" type="text" value={this.state.value} onChange={this.handleChange}/>
    }
}

SearchField.propTypes = {
    search: React.PropTypes.object,
    onSearchFieldClick: React.PropTypes.func,
    onSearchPopoverItemClick: React.PropTypes.func
};

import 'babel-polyfill'
import React from 'react'
import AppBarWithActions from '../../containers/AppBarWithActions'
import Layout from '../Layout'
import ProductsContainer from '../../containers/ProductsContainer'
import CategoriesContainer from '../../containers/CategoriesContainer'
import SolutionsContainer from '../../containers/SolutionsContainer'
import EditableSolution from '../../containers/EditableSolution'
import NewSolutionContainer from '../../containers/NewSolutionContainer'
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router'
import {Tabs, Tab} from 'material-ui/Tabs'

// Material-UI Theme
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#D50202',
        accent1Color: '#FFEB3B'
    }
})

const Root = () => (
    <MuiThemeProvider muiTheme={muiTheme}>
        <Router history={browserHistory}>
            <Route path="/" component={Layout}>
                <IndexRoute component={CategoriesContainer}/>

                <Route path="/categories" component={CategoriesContainer} />
                <Route path="/products" component={ProductsContainer} />
                <Route path="/solutions" component={SolutionsContainer} />
                <Route path="/solutions/new" component={NewSolutionContainer} />
                <Route path="/solutions/:id" component={EditableSolution} />
            </Route>
        </Router>

    </MuiThemeProvider>
)

export default Root

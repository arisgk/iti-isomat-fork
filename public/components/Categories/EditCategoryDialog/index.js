import React, {Component} from 'react'
import Dialog from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import CircularProgress from 'material-ui/CircularProgress'

const styles = {
    container: {
        maxWidth: 'none',
        width: '60%'
    },
    spinner: {
        position: 'absolute',
        top: 16,
        right: 24
    }
}

class EditCategoryDialog extends Component {

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        })
    }

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            title: '',
            parentid: '',
            color: '',
            bgcolor: '',
            language: '',
            children: [],
            products: '',
            ordering: ''
        }
    }

    componentWillReceiveProps (nextProps) {
        let { category } = nextProps

        if (category) {
            this.setState({
                _id: category._id,
                id: category.id,
                title: category.title,
                parentid: category.parentid,
                color: category.color,
                bgcolor: category.bgcolor,
                language: category.language,
                children: category.children,
                products: category.products,
                ordering: category.ordering
            })
        }
    }

    render() {

        let {
            category,
            onCancelEditing,
            onSave,
            isUpdatingCategory
        } = this.props

        const actions = [
            <FlatButton
                label="Cancel"
                onTouchTap={() => onCancelEditing()}
            />,
            <FlatButton
                label="Save"
                primary={true}
                onTouchTap={() => onSave(category, this.state)}
            />
        ]

        return (
            <Dialog
                title={category ? category.title : "Category"}
                actions={actions}
                modal={false}
                contentStyle={styles.container}
                open={category ? true : false}
                onRequestClose={() => onCancelEditing()}
                autoScrollBodyContent={true}
            >
                <CircularProgress size={0.4} className={isUpdatingCategory ? "visible" : "visibility-hidden"} style={styles.spinner} />

                <TextField name="id" floatingLabelText={"ID"} floatingLabelFixed={true} value={this.state.id} onChange={this.handleChange} fullWidth={true}     />
                <TextField name="title" floatingLabelText={"Title"} floatingLabelFixed={true} value={this.state.title} onChange={this.handleChange} fullWidth={true} />
                <TextField name="parentid" floatingLabelText={"Parent ID"} floatingLabelFixed={true} value={this.state.parentid} onChange={this.handleChange} fullWidth={true} />
                <TextField name="color" floatingLabelText={"Color"} floatingLabelFixed={true} value={this.state.color} onChange={this.handleChange} fullWidth={true} />
                <TextField name="bgcolor" floatingLabelText={"Background Color"} floatingLabelFixed={true} value={this.state.bgcolor} onChange={this.handleChange} fullWidth={true} />
                <TextField name="language" floatingLabelText={"Language"} floatingLabelFixed={true} value={this.state.language} onChange={this.handleChange} fullWidth={true} />
                <TextField name="products" floatingLabelText={"Products"} floatingLabelFixed={true} value={this.state.products} onChange={this.handleChange} fullWidth={true} />
                <TextField name="ordering" floatingLabelText={"Ordering"} floatingLabelFixed={true} value={this.state.ordering} onChange={this.handleChange} fullWidth={true} />
            </Dialog>
        )
    }
}

export default EditCategoryDialog

import React, {Component} from 'react'
import Dialog from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import CircularProgress from 'material-ui/CircularProgress'

const styles = {
    container: {
        maxWidth: 'none',
        width: '60%'
    },
    spinner: {
        position: 'absolute',
        top: 16,
        right: 24
    }
}

class AddCategoryDialog extends Component {

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        })
    }

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            title: '',
            parentid: '',
            color: '',
            bgcolor: '',
            language: '',
            children: [],
            products: '',
            ordering: ''
        }
    }

    render() {

        let {
            onCancelAdding,
            onSaveNew,
            isAddingCategory,
            isUpdatingCategory
        } = this.props

        const actions = [
            <FlatButton
                label="Cancel"
                onTouchTap={() => onCancelAdding()}
            />,
            <FlatButton
                label="Save"
                primary={true}
                onTouchTap={() => onSaveNew(this.state)}
            />
        ]

        return (
            <Dialog
                title={"New Category"}
                actions={actions}
                modal={false}
                contentStyle={styles.container}
                open={isAddingCategory ? true : false}
                onRequestClose={() => onCancelAdding()}
                autoScrollBodyContent={true}
            >
                <CircularProgress size={0.4} className={isUpdatingCategory ? "visible" : "visibility-hidden"} style={styles.spinner} />

                <TextField name="id" floatingLabelText={"ID"} floatingLabelFixed={true} value={this.state.id} onChange={this.handleChange} fullWidth={true}     />
                <TextField name="title" floatingLabelText={"Title"} floatingLabelFixed={true} value={this.state.title} onChange={this.handleChange} fullWidth={true} />
                <TextField name="parentid" floatingLabelText={"Parent ID"} floatingLabelFixed={true} value={this.state.parentid} onChange={this.handleChange} fullWidth={true} />
                <TextField name="color" floatingLabelText={"Color"} floatingLabelFixed={true} value={this.state.color} onChange={this.handleChange} fullWidth={true} />
                <TextField name="bgcolor" floatingLabelText={"Background Color"} floatingLabelFixed={true} value={this.state.bgcolor} onChange={this.handleChange} fullWidth={true} />
                <TextField name="language" floatingLabelText={"Language"} floatingLabelFixed={true} value={this.state.language} onChange={this.handleChange} fullWidth={true} />
                <TextField name="products" floatingLabelText={"Products"} floatingLabelFixed={true} value={this.state.products} onChange={this.handleChange} fullWidth={true} />
                <TextField name="ordering" floatingLabelText={"Ordering"} floatingLabelFixed={true} value={this.state.ordering} onChange={this.handleChange} fullWidth={true} />
            </Dialog>
        )
    }
}

export default AddCategoryDialog

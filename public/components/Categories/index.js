import React from 'react'
import CategoriesTable from './CategoriesTable'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import EditCategoryDialog from './EditCategoryDialog'
import AddCategoryDialog from './AddCategoryDialog'

const styles = {
    fab: {
        position: 'fixed',
        bottom: 64,
        right: 24
    }
}

class CategoriesTab extends React.Component {

    getEditingCategory (categories) {
        if (categories) {
            for (let category of categories) {
                if (category.isEditing) return category
            }
        }

        return null
    }

    render () {

        let {
            categories,
            fetchData,
            onPreviousPageButtonClick,
            onNextPageButtonClick,
            onNewCategoryClick,
            onCategorySelection,
            onDeleteButtonClick,
            onEditButtonClick,
            onSave,
            onCancelEditing,
            onSaveNew,
            onCancelAdding
        } = this.props

        return (
            <div className="categories-tab-container">

                <CategoriesTable
                    categories={categories}
                    onPreviousPageButtonClick={onPreviousPageButtonClick}
                    onNextPageButtonClick={onNextPageButtonClick}
                    onCategorySelection={onCategorySelection}
                    fetchData={fetchData}
                    onEditButtonClick={onEditButtonClick}
                    onDeleteButtonClick={onDeleteButtonClick}
                />

                <FloatingActionButton style={styles.fab} onTouchTap={() => onNewCategoryClick()} >
                    <ContentAdd />
                </FloatingActionButton>

                <EditCategoryDialog category={this.getEditingCategory(categories.data)} isUpdatingCategory={categories.isUpdatingCategory} onSave={onSave} onCancelEditing={onCancelEditing} />
                <AddCategoryDialog isAddingCategory={categories.isAddingCategory} isUpdatingCategory={categories.isUpdatingCategory} onSaveNew={onSaveNew} onCancelAdding={onCancelAdding} />
            </div>
        )
    }

}

export default CategoriesTab

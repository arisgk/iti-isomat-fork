import React from 'react'

const style = {
	color: 'rgba(0,0,0,0.54)',
	fontWeight: 700
}

const pageItems = 50

const ItemsCountLabel = ({ categories }) => {

	if (categories && categories.data && categories.page && categories.itemCount) {

		const startIndex = ((categories.page - 1) * pageItems) + 1
		const endIndex = startIndex + (categories.data.length - 1)
		const itemCount = categories.itemCount

		return (
			<span style={style}>
				{`${startIndex}-${endIndex} of ${itemCount}`}
			</span>
		)
	} else {
		return (
			<span></span>
		)
	}
}


export default ItemsCountLabel

import React from 'react'
import ItemsCountLabel from './ItemsCountLabel'
import IconButton from 'material-ui/IconButton'

const styles = {
	cell: {
		textAlign: 'right'
	},
	iconButton: {
	    width: '32px',
	    height: '32px',
	    padding: 0
	}
}

const TableFooterControls = ({ categories, onPreviousPageClick, onNextPageClick }) => (
	<td styles={styles.cell} className="row">
		<div className="col-xs-8 col-xs-offset-4 col-md-6 col-md-offset-6 col-lg-2 col-lg-offset-10 table-footer-controls-container">
			<span className="items-count-label">
				<ItemsCountLabel categories={categories} />
			</span>

			<span className="pagination-controls">
				<IconButton style={styles.iconButton} onClick={onPreviousPageClick} disabled={(categories.page <= 1)}>
					<i className="material-icons pagination-icon">chevron_left</i>
				</IconButton>
				<IconButton style={styles.iconButton} onClick={onNextPageClick} disabled={(categories.page >= categories.pageCount)}>
					<i className="material-icons pagination-icon">chevron_right</i>
				</IconButton>
			</span>
		</div>
	</td>
)


export default TableFooterControls

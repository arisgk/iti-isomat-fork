import React from 'react'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter } from 'material-ui/Table'
import IconButton from 'material-ui/IconButton'
import TableFooterControls from './TableFooterControls'
import CircularProgress from 'material-ui/CircularProgress'
import $ from 'jquery'

const styles = {
	iconButton: {
	    width: '32px',
	    height: '32px',
	    padding: 0
	},
	tableFooterRow: {
        textAlign: 'right'
    }
}

// Categories table presentational component. Based on Material-UI table.
class CategoriesTable extends React.Component {

    componentDidMount () {

        let { fetchData } = this.props
        fetchData()
    }

	calculateTableHeight () {
		const appBarHeight = 64
		const tableTitleHeight = 64
		const tableHeaderHeight = 57
		const tableFooterHeight = 48

		return window.innerHeight - (appBarHeight + tableTitleHeight + tableHeaderHeight + tableFooterHeight + 8)
	}

	getSelectedCategory (categories) {
		if (categories) {
			for (let category of categories) {
				if (category.selected) return category
			}
		}

		return null
	}

    render () {

        let { categories, onPreviousPageButtonClick, onNextPageButtonClick, onCategorySelection, onEditButtonClick, onDeleteButtonClick } = this.props

        let data = categories.data
		let isFetchingCategories = categories.isFetchingCategories

		let tableHeight = `${this.calculateTableHeight()}px`
		let selectedCategory = this.getSelectedCategory(data)

        return (
            <div className="categories-table-container">

                <div className="table-title-container">
					<div className="title-and-spinner">
						<h2>Categories</h2>
						<CircularProgress size={0.4} className={isFetchingCategories ? "visible" : "visibility-hidden"} />
					</div>
					<IconButton className="delete-button" style={styles.iconButton} disabled={!selectedCategory} onTouchTap={() => onDeleteButtonClick(selectedCategory)}>
    					<i className="material-icons">delete</i>
    				</IconButton>
                    <IconButton className="edit-button" style={styles.iconButton} disabled={!selectedCategory} onTouchTap={() => onEditButtonClick()}>
    					<i className="material-icons">mode_edit</i>
    				</IconButton>
                </div>

                <Table fixedHeader={true} fixedFooter={true} height={tableHeight} onRowSelection={(selectedRows) => onCategorySelection(data[selectedRows[0]])}>
                    <TableHeader>
                        <TableRow>
                            <TableHeaderColumn>ID</TableHeaderColumn>
                            <TableHeaderColumn>Title</TableHeaderColumn>
                            <TableHeaderColumn>Parent ID</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        {data.map(category =>
                            <TableRow key={category.id} selected={category.selected}>
                                <TableRowColumn>{category.id}</TableRowColumn>
                                <TableRowColumn>{category.title}</TableRowColumn>
                                <TableRowColumn>{category.parentid}</TableRowColumn>
                            </TableRow>
                        )}
                    </TableBody>
					<TableFooter style={styles.tableFooter}>
	                    <TableRow style={styles.tableFooterRow}>
	                        <TableFooterControls
								categories={categories}
	                            onPreviousPageClick={() => onPreviousPageButtonClick(categories)}
	                            onNextPageClick={() => onNextPageButtonClick(categories)}
	                        />
	                    </TableRow>
                	</TableFooter>
                </Table>
            </div>
        )
    }
}

export default CategoriesTable

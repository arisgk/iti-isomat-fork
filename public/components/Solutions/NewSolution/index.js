import React from 'react'
import TextField from 'material-ui/TextField'
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
    titleSpinnerContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleSpinnerInnerContainer: {
        display: 'flex',
        alignItems: 'center',
    },
    h2: {
        fontSize: 20,
        fontWeight: 500,
        color: 'rgba(0, 0, 0, 0.87)',
        textAlign: 'center',
        flex: 'initial',
    },
    container: {
        marginTop: 32
    },
    buttonContainer: {
        marginTop: 40,
        display: 'flex',
        flexDirection: 'row-reverse',
    },
}

class NewSolution extends React.Component {

    static contextTypes = {
        router: React.PropTypes.object,
    }

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            title: '',
            description: '',
            images: [],
            video: ''
        }

        this.handleChange = this.handleChange.bind(this)
    }

    handleChange (event) {
        this.setState({
            [event.target.name]: event.target.value,
        })
    }

    render() {
        const {
            isAddingSolution,
            onSaveNew
        } = this.props

        return(
            <div>
                <div style={styles.titleSpinnerContainer}>
                    <div style={styles.titleSpinnerInnerContainer}>
                        <h2 style={styles.h2}>New Solution</h2>
                        <CircularProgress size={0.4} className={isAddingSolution ? "visible" : "visibility-hidden"} />
                    </div>
                </div>

                <div style={styles.container} className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <TextField name="id" floatingLabelText={"ID"} floatingLabelFixed={true} value={this.state.id} onChange={this.handleChange} fullWidth={true} />
                            <TextField name="title" floatingLabelText={"Title"} floatingLabelFixed={true} value={this.state.title} onChange={this.handleChange} fullWidth={true} />
                            <TextField name="description" floatingLabelText={"Description"} floatingLabelFixed={true} value={this.state.description} onChange={this.handleChange} fullWidth={true} />
                            <TextField name="video" floatingLabelText={"Video"} floatingLabelFixed={true} value={this.state.video} onChange={this.handleChange} fullWidth={true} />

                            <div style={styles.buttonContainer}>
                                <RaisedButton label="Add" primary={true} onTouchTap={() => onSaveNew(this.state, this.context.router)} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default NewSolution

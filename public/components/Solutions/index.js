import React from 'react'
import SolutionCard from './SolutionCard'
import CircularProgress from 'material-ui/CircularProgress'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'

const styles = {
    titleSpinnerContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleSpinnerInnerContainer: {
        display: 'flex',
        alignItems: 'center',
    },
    h2: {
        fontSize: 20,
        fontWeight: 500,
        color: 'rgba(0, 0, 0, 0.87)',
        textAlign: 'center',
        flex: 'initial',
    },
    container: {
        marginTop: 32
    },
    cardContainer: {
        marginBottom: 32
    },
    fab: {
        position: 'fixed',
        bottom: 40,
        right: 24
    }
}

class Solutions extends React.Component {

    static contextTypes = {
        router: React.PropTypes.object,
    }

    constructor(props) {
        super(props)
        this.handleNewClick = this.handleNewClick.bind(this)
    }

    handleNewClick(event) {
        const { router } = this.context

        router.push(`/solutions/new`)
    }

    componentDidMount () {

        let { fetchData } = this.props
        fetchData()
    }

    render() {

        let { solutions } = this.props

        let data = solutions.data
		let isFetchingSolutions = solutions.isFetchingSolutions

        return (
            <div>
                <div style={styles.titleSpinnerContainer}>
                    <div style={styles.titleSpinnerInnerContainer}>
                        <h2 style={styles.h2}>Solutions</h2>
                        <CircularProgress size={0.4} className={isFetchingSolutions ? "visible" : "visibility-hidden"} />
                    </div>
                </div>

                <div style={styles.container} className="container">
                    <div className="row">
                        {data.map(solution =>
                            <div style={styles.cardContainer} className="col-md-4" key={`solution-${solution.id}`}>
                                <SolutionCard solution={solution} />
                            </div>
                        )}
                    </div>
                </div>

                <FloatingActionButton style={styles.fab} onTouchTap={this.handleNewClick} >
                    <ContentAdd />
                </FloatingActionButton>
            </div>
        )
    }
}

export default Solutions

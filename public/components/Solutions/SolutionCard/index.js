import React from 'react'
import path from 'path'
import { Card, CardMedia, CardTitle } from 'material-ui/Card'

const styles = {
    card: {
        cursor: 'pointer',
    },
    title: {
        textAlign: 'center',
    },
}

class SolutionCard extends React.Component {

    static contextTypes = {
        router: React.PropTypes.object,
    }

    constructor(props, context) {
        super(props, context)
        this.handleCardClick = this.handleCardClick.bind(this)
    }

    handleCardClick(item, event) {
        const { solution } = this.props
        const { router } = this.context

        router.push(`/solutions/${solution._id}`)
    }

    render() {
        const { solution } = this.props

        return(
            <Card style={styles.card} className="card" onTouchTap={this.handleCardClick}>
                <CardMedia>
                    <img src="/images/solutions.jpg" />
                </CardMedia>
                <CardTitle style={styles.title} title={solution.title} />
            </Card>
        )
    }
}

export default SolutionCard

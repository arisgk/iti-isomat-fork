import React from 'react'
import TextField from 'material-ui/TextField'
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton'
import Dropzone from 'react-dropzone'
import { GridList, GridTile } from 'material-ui/GridList'
import path from 'path'
import IconButton from 'material-ui/IconButton'

const styles = {
    titleSpinnerContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleSpinnerInnerContainer: {
        display: 'flex',
        alignItems: 'center',
    },
    h2: {
        fontSize: 20,
        fontWeight: 500,
        color: 'rgba(0, 0, 0, 0.87)',
        textAlign: 'center',
        flex: 'initial',
    },
    container: {
        marginTop: 32
    },
    gridListContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        marginTop: 32,
    },
    gridList: {
        width: '100%',
        height: 'auto',
        overflowY: 'auto',
        marginBottom: 24,
    },
    buttonContainer: {
        marginTop: 40,
        display: 'flex',
        flexDirection: 'row-reverse',
    },
    actionIconButton: {
        color: 'white'
    },
    dropzone: {
        border: '1px solid rgba(0, 0, 0, 0.54)',
        width: 200,
        height: 200,
        borderRadius: 6,
        cursor: 'pointer'
    },
    dropzoneContentContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        height: '100%'
    },
    addPhotoIconContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    addPhotoIcon: {
        color: 'rgba(0,0,0,0.54)',
        fontSize: 72
    },
    addPhotoTextContainer: {
        color: 'rgba(0,0,0,0.54)',
        textAlign: 'center',
        marginTop: -8,
        paddingBottom: 8,

    }
}

class SolutionDetails extends React.Component {

    constructor(props) {
        super(props);

        const { solution } = props

        if (solution) {
            this.state = {
                _id: solution._id || '',
                id: solution.id || '',
                title: solution.title || '',
                description: solution.description || '',
                images: solution.images || [],
                video: solution.video || ''
            }
        } else {
            this.state = {
                _id: '',
                id: '',
                title: '',
                description: '',
                images: [],
                video: ''
            }
        }

        this.handleChange = this.handleChange.bind(this)
        this.onOpenClick = this.onOpenClick.bind(this)
    }

    componentDidMount () {

        let { solution, fetchSolution, params } = this.props

        if (!solution) {
            fetchSolution(params.id)
        }
    }

    handleChange (event) {
        this.setState({
            [event.target.name]: event.target.value,
        })
    }

    componentWillReceiveProps (nextProps) {
        let { solution } = nextProps

        if (solution) {
            this.setState({
                _id: solution._id,
                id: solution.id,
                title: solution.title,
                description: solution.description,
                images: solution.images,
                video: solution.video,
            })
        }
    }

    onOpenClick() {
      this.refs.dropzone.open();
    }

    render() {
        const {
            solution,
            isUpdatingSolution,
            onSave,
            onImageDrop,
            onImageDelete,
        } = this.props

        return(
            <div>
                <div style={styles.titleSpinnerContainer}>
                    <div style={styles.titleSpinnerInnerContainer}>
                        <h2 style={styles.h2}>{solution ? solution.title : 'Solution'}</h2>
                        <CircularProgress size={0.4} className={isUpdatingSolution ? "visible" : "visibility-hidden"} />
                    </div>
                </div>

                <div style={styles.container} className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <TextField name="id" floatingLabelText={"ID"} floatingLabelFixed={true} value={this.state.id} onChange={this.handleChange} fullWidth={true} />
                            <TextField name="title" floatingLabelText={"Title"} floatingLabelFixed={true} value={this.state.title} onChange={this.handleChange} fullWidth={true} />
                            <TextField name="description" floatingLabelText={"Description"} floatingLabelFixed={true} value={this.state.description} onChange={this.handleChange} fullWidth={true} />
                            <TextField name="video" floatingLabelText={"Video"} floatingLabelFixed={true} value={this.state.video} onChange={this.handleChange} fullWidth={true} />

                            <div style={styles.gridListContainer}>
                                <GridList
                                    cellHeight={200}
                                    cols={4}
                                    padding={24}
                                    style={styles.gridList}
                                >
                                    {solution ? solution.images.map((image) => (
                                        <GridTile
                                            key={image}
                                            title=" "
                                            titlePosition="top"
                                            titleBackground="linear-gradient(to bottom, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
                                            actionIcon={
                                                <IconButton style={styles.actionIconButton} onTouchTap={() => onImageDelete(image, solution)}>
                                                    <i className="material-icons">clear</i>
                                                </IconButton>
                                            }
                                            actionPosition="right"
                                        >
                                            <img src={path.relative('/public/components/Solutions/SolutionDetails/index.js', image)} role="presentation" />
                                        </GridTile>
                                    )) : null }
                                </GridList>

                                <Dropzone ref="dropzone" accept={"image/*"} onDrop={ files => onImageDrop(files[0], solution) } multiple={false} style={styles.dropzone}>
                                    <div style={styles.dropzoneContentContainer}>
                                        <div style={styles.addPhotoTextContainer}>Drag or Click to Upload</div>
                                        <div style={styles.addPhotoIconContainer}>
                                            <i className="material-icons" style={styles.addPhotoIcon}>add_a_photo</i>
                                        </div>
                                    </div>
                                </Dropzone>
                            </div>

                            <div style={styles.buttonContainer}>
                                <RaisedButton label="Save" primary={true} onTouchTap={() => onSave(solution, this.state)} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SolutionDetails

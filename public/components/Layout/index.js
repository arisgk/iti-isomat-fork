import React from 'react'
import AppBarWithActions from '../../containers/AppBarWithActions'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'

const styles = {
    drawerHeader: {
        height: 64,
        backgroundColor: '#D50202',
        display: 'flex',
        alignItems: 'center',
    },
    drawerLogo: {
        paddingLeft: 24
    }
}

class Layout extends React.Component {

    static contextTypes = {
        router: React.PropTypes.object,
    }

    constructor (props) {
        super(props)
        this.state = {open: false}
    }

    handleToggle = () => this.setState({open: !this.state.open})

    handleClose = () => this.setState({open: false})

    handleMenuItemTouchTap = (item, event) => {
        const { router } = this.context

        switch (item) {
            case 'Categories':
                router.push('/categories')
                break;
            case 'Products':
                router.push('/products')
                break;
            case 'Solutions':
                router.push('/solutions')
                break;
            default:
                router.push('/categories')
        }

        this.handleClose()
    }

    render () {

        let { children, location } = this.props

        return (
            <div>
                <AppBarWithActions currentRoute={location.pathname} onLeftIconButtonTouchTap={this.handleToggle} />

                <Drawer
                  docked={false}
                  open={this.state.open}
                  onRequestChange={(open) => this.setState({open})}
                >
                    <div style={styles.drawerHeader} onTouchTap={this.handleTouchTapHeader}>
                        <img style={styles.drawerLogo} src="../../images/isomat-logo.png" />
                    </div>

                    <MenuItem onTouchTap={this.handleMenuItemTouchTap.bind(this, "Categories")} primaryText="Categories" />
                    <MenuItem onTouchTap={this.handleMenuItemTouchTap.bind(this, "Products")} primaryText="Products" />
                    <MenuItem onTouchTap={this.handleMenuItemTouchTap.bind(this, "Solutions")} primaryText="Solutions" />
                </Drawer>
                {children}
            </div>
        )
    }
}

export default Layout

var json2csv = require('json2csv');
var fs = require('fs');

var tasks = {};

tasks.createCsvFile = function(metrics, metricName){

    var fields = ['Metric', 'Company', 'Year', 'Value', 'Source'];

    json2csv({ data: metrics, fields: fields }, function(err, csv) {
        if (err) console.log(err);

        fs.writeFile(metricName +'.csv', csv, function(err) {
        if (err) throw err;
            console.log('file saved');
        });
    });
}

module.exports = tasks;
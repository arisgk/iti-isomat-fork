"use strict"
const mkdirp = require('mkdirp')
const async = require('async')
const http = require('http')
const fs = require('fs')
const path = require('path')
const download = require('url-download')
const request = require('request')
const youtubedl = require('youtube-dl')
const archiver = require('archiver')
const jsonfile = require('jsonfile')
const multer  = require('multer')
const mime = require('mime')
const crypto = require('crypto')
const del = require('delete')

let tasks = {}

/*
    Ensures that the directories for the imported files exist.
    If they don't it creates them.
 */
tasks.ensureDirectories = (callback) => {

    async.series([

        function(callback){

            mkdirp('./public/imports/images', (err) => {
                if (err) {
                    return callback(err)
                }

                callback()
            })
        },
        function(callback){

            mkdirp('./public/imports/pdf', (err) => {
                if (err) {
                    return callback(err)
                }

                callback()
            })
        },
        function(callback){

            mkdirp('./public/imports/videos', (err) => {
                if (err) {
                    return callback(err)
                }

                callback()
            })
        }
    ],
    function(err, results){

        if (err) {
            return callback(err)
        }

        callback()
    })
}

const downloadFile = function(url, destination, asyncCallback) {

  request.get({url: url, encoding: 'binary'}, function (err, response, body) {
      fs.writeFile(destination, body, 'binary', function(err) {

        if (err){

            console.log('File download error : ' + err);
        } else {

            console.log('File saved successfully.');
        }
        asyncCallback();
      });
    });
};

const downloadImage = function (url, destination, asyncCallback){

    download(url, destination)
        .on('close', function () {
            console.log('Image downloaded successfully.');
            asyncCallback();
        });
}

const downloadVideo = function (url, destination, asyncCallback){

    const video = youtubedl(url, ['--format=18'] ,{cwd: __dirname});

    // Will be called when the download starts.
    video.on('info', function(info) {
        console.log('Download started')
        console.log('filename: ' + info.filename)
        console.log('size: ' + info.size)
    });

    video.on('complete', function complete(info) {
        console.log('filename: ' + info._filename + ' already downloaded.')
    });

    video.on('end', function() {
        console.log('finished downloading!')
        asyncCallback();
    });

    video.pipe(fs.createWriteStream(destination));
}

tasks.saveFilesLocally = function(file, asyncCallback){

    async.parallel([

        callback => {

            if (file.pdf){

                file.pdfLocal = path.join('./public/imports/pdf/', path.basename(file.pdf));
                downloadFile(file.pdf, path.join('./public/imports/pdf/', path.basename(file.pdf)) , callback)
            } else {

                callback()
            }
        },
        callback => {

            if (file.image){

                file.imageLocal = path.join('./public/imports/images/', path.basename(file.image));
                downloadImage(file.image, path.join('./public/imports/images/') , callback)
            } else {

                callback()
            }
        },
        callback => {

            if (file.video){

                file.videoLocal = path.join('./public/imports/videos/', path.basename(file.video));
                downloadVideo(file.video, path.join('./public/imports/videos/'), path.basename(file.video), callback)
            } else {

                callback()
            }
        }
    ],
    (err, results) => {
        if (err) {

            console.log('Error in saving files ' + err);
        } else {

            file.save(function (err) {

                if (err) {

                    console.log('Error in saving product.' + file.title);
                }
                asyncCallback();
            });
        }
    });
}

tasks.exportToZip = function (req, res, next){

    const output = fs.createWriteStream('./public/export.zip');
    let archive = archiver('zip');

    output.on('close', function() {
        console.log(archive.pointer() + ' total bytes');
        console.log('archiver has been finalized and the output file descriptor has closed.');
        next();
    });

    archive.on('error', function(err) {
        console.log('Error when creating zip export ' + err);
        throw err;
    });

    const exportJsonFile = './export.json';

    archive.pipe(output);

    archive
      .directory('./public/imports/')
      .directory('./public/uploads/')
      .append(fs.createReadStream(exportJsonFile), { name: 'export.json'})
      .finalize();
}

tasks.writeExportToJsonFile = function (exportJson, next){

    var file = './export.json'

    jsonfile.writeFile(file, exportJson, function (err) {
        if (err){
            console.log('Error when writing exportJson to json file ' + err)
        } else {
            console.log('Successfully wrote exportJson to json file.')
        }
        return next();
    })
}

tasks.uploadImage = function () {

    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            const dir = './public/uploads/images/'
            mkdirp(dir, err => cb(err, dir))
        },
        filename: function (req, file, cb) {

            if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                return cb(new Error('Only image files are allowed!'))
            }

            crypto.pseudoRandomBytes(16, function (err, raw) {
                cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
            });
        }
    })

    const upload = multer({ storage: storage })
    return upload.single('file')
}

tasks.deleteImage = function (req, res, next) {
    del(req.query.image, function(err) {
        if (err) {
            return next(err)
        }

        next()
    })
}

module.exports = tasks
